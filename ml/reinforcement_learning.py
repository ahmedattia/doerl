
"""
Components of reinfocement learning
"""

from ..utility import utility

import numpy as np

class Policy(object):
    def __init__(self, *args, **kwargs):
        """
        A class defining a Reinfocement Learning policy
        """
        raise NotImplementedError

    def update_policy_parameters(self, *args, **kwargs):
        """
        """
        raise NotImplementedError

    def sample_action(self, *args, **kwargs):
        """
        Use the policy probability distribution to sample a single action
        """
        raise NotImplementedError

    def sample_trajectory(self, *args, **kwargs):
        """
        Use the policy probability distribution to sample a trajectory, starting  from the passed initial state

        Args:
            init_state
            length: length of the trajectory; nuumber of state-action pairs to be sampled

        Returns:
            trajectory: list contining state-action pairs [(state, action), ...(state, action)]
                the action associated with last state should do nothing to the corresponding state
                number of entries in the trajectory is equal to the passed length+1, i.e., 'length' state
                are generated  after the initial state  which is used in the first pair in
                the trajectory.

        """
        raise NotImplementedError

    def conditional_probability(self, *args, **kwargs):
        """
        Calculate the probability of s1 conditioned by s0; i.e. $p(s1|s0)$
        """
        raise NotImplementedError

    def conditional_log_probability_gradient(self, *args, **kwargs):
        """
        Calculate the gradient of log-probability of s1 conditioned by s0; i.e. $p(s1|s0)$, w.r.t. parameters
        """
        raise NotImplementedError

    @property
    def hyperparameters(self):
        return self._hyperparameters

class State(object):
    def __init__(self, *args, **kwargs):
        """
        A class defining a Reinfocement Learning State for the environment
        """
        raise NotImplementedError

    @property
    def size(self):
        """
        Size of the internal state
        """
        return self._state.size
    @property
    def state(self):
        """
        Retrieve the internal state
        """
        return self._state
    @state.setter
    def state(self, state):
        """
        Update the internal state, after assertion
        """
        if isinstance(State):
            assert state.size == self.size, 'passed state must be of equal size to the internal state!'
            self._state = state.copy()  # compy to avoid external manipulation
        elif utility.isiterable(state):
            assert len(state)== self.size, 'passed state must be of equal size to the internal state!'
            self._state[...] = np.array(state).flatten()
        else:
            print("Can't used passed value to udpate internal state. Wrong Type detected!")
            raise TypeError
        raise NotImplementedError

class Action(object):
    def __init__(self):
        """
        A class defining a Reinfocement Learning Action
        """
        raise NotImplementedError

    @property
    def size(self):
        """
        Size of the internal action
        """
        return self._action.size
    @property
    def action(self):
        """
        Retrieve the internal action
        """
        return self._action
    @action.setter
    def action(self, action):
        """
        Update the internal actiion, after assertion
        """
        raise NotImplementedError

class Agent(object):
    def __init__(self, *args, **kwargs):
        """
        A class defining a Reinfocement Learning Agent
        """
        raise NotImplementedError

    def trajectory_return(self, *args, **kwargs):
        """
        given a trajectory; sequence/list of state-action  pairs, calculated the total reward
        Given the current state, and action, return the value of the reward function. Inspect policy if needed
        """
        raise NotImplementedError

    def reward(self, *args, **kwargs):
        """
        Given the current state, and action, return the value of the reward function. Inspect policy if needed
        """
        raise NotImplementedError

    def initialize_state(self, *args, **kwargs):
        """
        Initialize the state of the environment
        """
        raise NotImplementedError

    def update_state(self, *args, **kwargs):
        """
        Update the environment state to the passed state
        """
        raise NotImplementedError

    def sample_trajectory(self, *args, **kwargs):
        """
        Sample a trajectory as long as the passed length
        """
        raise NotImplementedError

    def policy_gradient(self, *args, **kwargs):
        """
        Calculate policy gradient
        """
        raise NotImplementedError

    def train(self, *args, **kwargs):
        """
        Train the agent to optimize the policy
        """
        raise NotImplementedError

    @property
    def policy(self):
        """
        Handle to the underlying policy
        """
        return self._policy

class BernoulliPolicy(Policy):
    def __init__(self, size, theta=0.5):
        """
        A class defining a Reinfocement Learning policy, based on Bernoulli distribution
        $p(x) = \theta^{x}\times (1-\theta)^{1-x}; \, x \in \{0, 1\}$

        Args:
            theta: p(x=1)

        """
        assert utility.isnumber(size), "size must be a number"
        assert int(size) == size, "size must be an  integer"
        self._state_size = size
        self._hyperparameters = dict()
        self.update_policy_parameters(theta=theta)

    def update_policy_parameters(self, theta):
        """
        update theta; that is p(x=1)
        """
        if utility.isnumber(theta):
            assert 0 <= theta <= 1, 'Parameter value %f is not allowd! theta must fall in the interval [0, 1]' % theta
            theta = np.ones(self._state_size, dtype=np.float) * theta

        elif utility.isiterable(theta):
            if len(theta) != self._state_size:
                print('parameter must be scalar or iterable of  length equal to problem size')
                print("Len(theta): %d , State size: %d " % (len(theta), self._state_size))
                raise ValueError
            theta = np.array(theta).flatten()
            if np.any(theta<0) or np.any(theta>1):
                print("Parameter value %f is not allowd! theta must fall in the interval [0, 1]")
                raise ValueError
        else:
            print("theta must be scalr or iterable of size :%d" % self._state_size)
            raise TypeError
        self._hyperparameters.update({'theta':theta})

    def sample_action(self):
        """
        Sample an appropriate action
        """
        size = self._state_size
        theta = self._hyperparameters['theta']
        action = FlipAction(size)
        sampled_action = np.array([np.random.binomial(1, theta[i]) for i in range(theta.size)]).astype(np.bool)
        # print("Sampled Action : ", sampled_action)
        # print("Under Policy: ", theta)
        action.action[...] = sampled_action
        return action

    def sample_trajectory(self, init_state, length):
        """
        Use the policy probability distribution to sample a trajectory, starting  from the passed initial state

        Args:
            init_state
            length: length of the trajectory; nuumber of state-action pairs to be sampled

        Returns:
            trajectory: list contining state-action pairs [(state, action), ...(state, action)]
                the action associated with last state should do nothing to the corresponding state
                number of entries in the trajectory is equal to the passed length+1, i.e., 'length' state
                are generated  after the initial state  which is used in the first pair in
                the trajectory.

        """
        state = init_state.copy()
        action = self.sample_action()
        trajectory = [(state, action)]
        for i in range(1, length+1):
            s = trajectory[i-1][0]
            a = trajectory[i-1][1]
            state = s.update(action=a, in_place=False)
            action = self.sample_action()
            if i == length:
                # set all entries of action to zeros
                action.action[...] = 0  # Better approach should be considered
            trajectory.append((state, action))
        return trajectory

    def conditional_probability(self, s0, s1, log_probability=True):
        """
        Calculate the probability of s1 conditioned by s0; i.e. $p(s1|s0)$
            Here, we assume iid entries of states
        Args:
            s0
            s1
            log_probability

        Returns:
            p: either p(s1|s0) or log p(s1|s0)

        """
        assert isinstance(s0, BinaryState) and isinstance(s0, BinaryState), 's0 and s1 must be instances of BinaryState'
        assert s0.size == s1.size, 's0 and s1 must be of equal sizes'
        # get parameter value (theta) for all state entries
        theta = self._hyperparameters['theta']
        assert theta.size == s0.size, "State size doesn't match the size of the parameter space!"

        # calculate log-probability
        diff = s1.state.astype(np.int) - s0.state.astype(np.int)
        diff_sqr = np.power(diff, 2)
        log_prob = diff_sqr * np.log(theta)
        log_prob -= (1.0 - diff_sqr) * np.log(1-theta)
        log_prob = np.sum(log_prob)

        # update if PMF value is required and return
        p = log_prob if log_probability else np.exp(log_prob)
        return p

    def conditional_log_probability_gradient(self, s0, s1):
        """
        Calculate the gradient of log-probability of s1 conditioned by s0; i.e. $p(s1|s0)$, w.r.t. parameters
            Here, we assume iid entries of states
        Args:
            s0
            s1

        Returns:
            log_prob_grad

        """
        assert isinstance(s0, BinaryState) and isinstance(s0, BinaryState), 's0 and s1 must be instances of BinaryState'
        assert s0.size == s1.size, 's0 and s1 must be of equal sizes'
        # get parameter value (theta) for all state entries
        theta = self._hyperparameters['theta']
        assert theta.size == s0.size, "State size doesn't match the size of the parameter space!"

        # calculate gradient of log-probability
        diff = s1.state.astype(np.int) - s0.state.astype(np.int)
        diff_sqr = np.power(diff, 2)

        log_prob_grad = diff_sqr / theta
        log_prob_grad += (diff_sqr - 1) / (1 - theta)
        return log_prob_grad

class BinaryState(State):
    def __init__(self, size):
        """
        A class defining a Reinfocement Learning State for the environment
        """
        assert utility.isnumber(size), "size must be a number"
        assert int(size) == size, "size must be an  integer"
        self._state = np.zeros(int(size), dtype=np.bool)

    def __copy__(self):
        """
        Copy this State
        """
        state_copy = BinaryState(size=self._state.size)
        state_copy._state = self._state.copy()
        return state_copy
    def copy(self):
        return self.__copy__()

    def update(self, action, in_place=True):
        """
        Given an action object, update the entries of the internal state
        update the state and either return self, or a copy with updated state
        """
        assert isinstance(action, FlipAction), 'action must be an instace of FlipAction'

        if in_place:
            out_state = self
        else:
            out_state = self.copy()
        out_state.state[action.action] = ~out_state.state[action.action]
        return out_state

class FlipAction(Action):
    def __init__(self, size):
        """
        A class defining a Reinfocement Learning Action
        An action is a binary vector that describes which state kept as is, and which is flipped

        """
        assert utility.isnumber(size), "size must be a number"
        assert int(size) == size, "size must be an  integer"
        self._action = np.zeros(int(size), dtype=np.bool)

    def __copy__(self):
        """
        Copy this Action instance
        """
        action_copy = BinaryState(size=self._actiion.size)
        action_copy._action = self._action.copy()
        return action_copy
    def copy(self):
        return self.__copy__()

    @property
    def size(self):
        """
        Size of the internal action
        """
        return self._action.size
    @property
    def action(self):
        """
        Retrieve the internal action
        """
        return self._action
    @action.setter
    def action(self, action):
        """
        Update the internal action, after assertion
        """
        raise NotImplementedError

class BinaryStateAgent(Agent):
    def __init__(self, problem_size, activation_penalty=1.0, trajectory_length=None, final_state_reward=None):
        """
        Intended to be developed gradually and used for binary sensor placement problem

        Args:
            problem_size: sets state and action size
            final_state_reward: a function handle to evaluate the oed criterion at the final state of a  trajectory
            activation_penalty: this value is used to penalize updating  entries of the state
                sensor updates: 0 -> 1 ==> reward = - activation_penalty
                sensor updates: 1 -> 0 ==> reward = + activation_penalty
                sensor updates: 0 -> 0 ==> reward = 0
                sensor updates: 1 -> 1 ==> reward = 0

        """
        # TODO: Consider changin activation penalty to be two values (one for turning on, and the other for off, with more weight to turning off
        assert utility.isnumber(problem_size), "problem_size must be a number"
        assert int(problem_size) == problem_size, "problem_size must be an integer"
        assert utility.isnumber(activation_penalty), "activation_penalty must be a number"
        self._problem_size = problem_size
        self._final_state_reward = final_state_reward
        self._activation_penalty = activation_penalty
        self._policy = BernoulliPolicy(self._problem_size)
        self._initial_state = self.initialize_state()
        self._state = self._initial_state.copy()
        if trajectory_length is None:
            print("WARNING: Infinite trajectory length is not supported. You'll have to pass a valid trajectory length for sampling later!")
            self._trajectory_length = None
        elif utility.isnumber(trajectory_length):
            assert int(trajectory_length) == trajectory_length, 'trajectory_length must be an integer!'
            self._trajectory_length = int(trajectory_length)
        else:
            print("trajectory_length must be a positive integer; received object of type %s" % type(trajectory_length))
            raise TypeError

    def trajectory_return(self, trajectory, regularization_penalty=None):
        """
        given a trajectory; sequence/list of state-action  pairs, calculated the total reward
        Given the current state, and action, return the value of the reward function. Inspect policy if needed

        Args:
            trajectory

        Returns:
            toal_reward
            to_go_returns: Returns calculated for all subtrajectories starting
                at state i of the trajectory.
                The returned list has length equal to the trajectory

        """
        total_reward = 0.0
        to_go_returns = np.empty(len(trajectory))
        # evaluate the reward of each state-action pair
        for i, [state, action] in enumerate(trajectory[: -1]):
            reward = self.reward(state, action)
            to_go_returns[i] = reward
            total_reward += reward
        # evaluate the OED criterion at the final state, and subtract it (we are maximizing total reward)
        if self._final_state_reward is not None:
            try:
                design = trajectory[-1][0].state.astype(np.int)
                final_reward = self._final_state_reward(design)
            except(TypeError):
                print("final_state_reward passed upon initialization is not valid!"
                      " Expection a function; found %s" % type(self._final_reward))
                raise
        else:
            final_reward = 0.0
        total_reward += final_reward

        to_go_returns = np.cumsum(to_go_returns[::-1])[::-1] + final_reward

        return total_reward, to_go_returns

    def reward(self, state, action):
        """
        Given the current state, and action, return the value of the reward function. Inspect policy if needed

        Args:
            state
            action

        Returns:
            r: state-action reward

        """
        # basic values
        new_state = state.update(action, in_place=False)
        state_size = state.size
        flip_locs = np.where(action.action)[0]
        num_flip = flip_locs.size
        s0 = state.state[flip_locs]  # sensors to be flipped
        num_keep = state_size - num_flip
        num_deactivate = np.count_nonzero(s0)  # number of active sensors to be shutdown
        num_activate = num_flip - num_deactivate
        #
        if False:
            print("State: ", state.state)
            print("Action: ", action.action)
            print("Flip-Locs: ", flip_locs)
            print("To be Flipped: ", s0)
            print("# sensors to Deactivate: ", num_deactivate)
            print("# sensors to Activate: ", num_activate)

        alpha = self._activation_penalty
        # TODO: Maybe pass two parameters instead of thie empirical double weighting!
        r = alpha * num_deactivate - alpha * num_activate  # better deactivate than activate!
        if num_flip>0: r /= num_flip


        # Only one in the following branches
        # Empirically, this is good for sparsification
        if True:
            # r += alpha * (state.size - np.count_nonzero(new_state.state))  # sparsity penalty
            r -= alpha * (np.count_nonzero(new_state.state)/state_size)  # sparsity penalty
            # r /= state.size
        else:
            # Empirically, this is perfect for exact number of sensors
            num_active_sensors = 7  # TODO: Add this to arguments...
            # r -= alpha *10* np.abs(np.count_nonzero(new_state.state)-num_active_sensors ) / state_size
            # r -= 2* alpha * (np.sum(new_state.state)-num_active_sensors )**2 / state_size
            r = 2* alpha * (np.sum(new_state.state)-num_active_sensors )**2 / state_size
        # print("Reward: > ", r)
        return r

    def initialize_state(self):
        """
        Initialize the state of the environment to all ones. All sensors are activee.
        """
        state = BinaryState(self._problem_size)
        state.state[...] = 1
        return state

    def update_state(self, state):
        """
        Update the environment state to the passed state
        """
        self._state.state = state

    def sample_trajectory(self, init_state=None, length=None):
        """
        Sample a trajectory as long as the passed length
            (if None is passed, the default trajectory length passed upon initialization is used).
            If the initial state is not passed, the initial state is set to the current state of the environment.
        """
        if length is None:
            if self._trajectory_length is None:
                print("Passed trajectory length is invalid, and failed to retrieve default trajectory length")
                raise ValueError
            else:
                length = self._trajectory_length
        else:
            assert utility.isnumber(length), "length must be a number"
            assert int(length) == length, "length must be an  integer"

        if init_state is None:
            init_state = self._state.copy()

        trajectory = self.policy.sample_trajectory(init_state=init_state, length=length)
        return trajectory

    def train(self, step_size=0.1, epochs=30, epoch_length=25, reward_to_go=False, verbose=False, rel_tol=1e-6):
        """
        Train the agent to optimize the associated policy

        Args:
            step_size: the learning rate, (step size in directorion of the gradient)
                Here, the gradient is normalized first,
            epochs: number of times gradient is calculated, and policy parameters are updated
            epoch_length: sample size, i.e., number of trajectories generated for each epoch, to calculate gradient
            reward_to_go: if True, the return is calculated forgetting the past in the policy gradient

        Returns:
            parameters_watch: a list of model parameters of length equal to the number of iterations,
                each entry shows the value of the policy parameters at a given iteration of the algorithm

        """
        # Apply REINFORCE algorithm to update the policy.
        # Other algorithms will be added gradually
        theta = self.policy.hyperparameters['theta'].copy()
        parameters_watch = [theta.copy()]
        rel_tol_watch = [np.NaN]
        for epoch in range(epochs):
            sample = [self.sample_trajectory() for i in range(epoch_length)]
            gradient = self.policy_gradient(sample, reward_to_go=reward_to_go, verbose=verbose)

            # update parameter; threshold close 0 and 1 to avoid issues with log(0) the gradient derivative
            if True:
                v = np.zeros_like(gradient)
                v[gradient>0] = 1
                d = v - theta
                new_theta = theta + step_size * d
            else:
                p_marg = 1e-9
                new_theta = theta + step_size * gradient
                new_theta[new_theta<p_marg] = p_marg
                new_theta[new_theta>1-p_marg] = 1-p_marg

                new_theta[np.isnan(new_theta)] = 0

            self.policy.update_policy_parameters(new_theta)

            # convergence
            update = new_theta-theta
            grad_norm = np.linalg.norm(gradient)
            update_rel_norm = np.abs(update).sum() / np.abs(theta).sum()
            rel_tol_watch.append(update_rel_norm)
            if verbose:
                print("Iteration: %d\t Graient Norm: %f; Update Rel Norm: %f " % (epoch, grad_norm, update_rel_norm))

            theta = new_theta.copy()
            parameters_watch.append(new_theta.copy())
            #
            if update_rel_norm <= rel_tol:
                print("Converged; Gradient Update Relative Norm < rel_to=%f " % rel_tol)
                break
        return parameters_watch, rel_tol_watch

    def policy_gradient(self, trajectories, reward_to_go=False, verbose=False):
        """
        Given a sample of trajectories, calculate the policy gradient, w.r.t policy parameter
        This initial implementation is based on MC approximation

        Args:
            trajectories: (list), with each entry is a trajectory of (state/action pairs)

        Returns:
            gradient

        """
        N = len(trajectories)  # number of trajectories (sample size)
        T = len(trajectories[0])  # length of the first trajectory
        for tr in trajectories:
            assert len(tr) == T, 'Not all trajectories in the sample have the same length!'

        gradient = None
        for tr in trajectories:
            # Total reward of the trajectory
            total_reward, to_go_returns = self.trajectory_return(tr, regularization_penalty=self._activation_penalty)

            # gradient of log-conditional-probability of the whole trajectory
            log_prob_grad = None
            for t in range(T-1):
                s0 = tr[t][0]
                s1 = tr[t+1][0]
                log_prob_grad_update = self.policy.conditional_log_probability_gradient(s0, s1)
                if reward_to_go:
                    log_prob_grad_update *= to_go_returns[t]
                else:
                    log_prob_grad_update *= total_reward

                if t == 0:
                    gradient_update = log_prob_grad_update
                else:
                    gradient_update += log_prob_grad_update

            # scale and update gradient
            if gradient is None:
                gradient = gradient_update
            else:
                gradient += gradient_update
        # scale by sample size (average) and return
        gradient = gradient / N

        # Scale gradient to max at 1
        # gradient /= np.abs(gradient).max()
        gradient /= np.abs(gradient).sum()

        if False:
            print("Gradient: ", gradient)
        return gradient



