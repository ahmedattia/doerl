
from doerl.utility import utility
import numpy as np
from scipy.stats import bernoulli as _sp_bernoulli
import itertools


def bernoulli_sample(p=0.5,
                     sample_size=32,
                     antithetic=False,
                     dtype=bool):
    """
    Sample a uniform random variable (1d or multivariate) according to probability
        p:=(P(x=1))
    If antithetic is True, sample_size must be even

    Args:
        sample_size
        p
        antithetic
        dtype

    Returns:
        bernoulli_sample: np array
            - if p is scalar or iterable of length 1, this will be 1d array of size=sample_size
            - if p is multivariate,  this will be 2d array wih each row, represinting one sample

    """
    assert sample_size>0 and int(sample_size)==sample_size, "sampel_size must be +ve integer"
    if antithetic:
        assert sample_size % 2 == 0, "antithetic requires even sample_size"

    p = np.array(p).flatten()
    dimension = p.size

    # Initialize container
    bernoulli_sample = np.empty((sample_size, dimension), dtype=dtype)

    for i in range(dimension):
        # sample uniform rv
        if antithetic:
            uniform = np.random.rand(sample_size//2)
        else:
            uniform = np.random.rand(sample_size)

        # First sample:
        num_ones = np.count_nonzero( uniform <= p[i] )
        # num_ones = np.count_nonzero( uniform < p[i] )

        if antithetic:
            # Second sample:
            num_ones += np.count_nonzero( (1-uniform) <= p[i] )
            # num_ones += np.count_nonzero( (1-uniform) < p[i] )

        num_zeros = sample_size - num_ones

        sample = np.zeros(sample_size, dtype=dtype)
        sample[: num_ones] = 1
        np.random.shuffle(sample)
        bernoulli_sample[:, i] = sample

    if dimension == 1:
        bernoulli_sample = bernoulli_sample.flatten()
    return bernoulli_sample

def sample(theta,
           sample_size,
           antithetic=False,
           return_np_array=True,
           dtype=bool):
    """
    """
    if True:  # TODO: Unify after debugging
        sample = bernoulli_sample(p=theta,
                                  sample_size=sample_size,
                                  antithetic=antithetic,
                                  dtype=dtype)
    else:
        theta = np.array(theta).flatten()
        sample =  np.zeros((theta.size, sample_size))
        for i in range(theta.size):
            sample[i, :] = _sp_bernoulli.rvs(theta[i], size=sample_size)
        sample =  sample.T

    # convert to list if needed
    if not return_np_array:
        sample = [s for s in sample]

    return  sample

def expect(theta, func, objective_value_tracker=None):
    """
    Calculate the expected value of a function (func) which accepts w as parameter
    """
    theta = np.array(theta).flatten()
    assert np.all(theta >= 0) and np.all(theta<=1), "theta must be in [0, 1]"
    size = theta.size
    if size > 15:
        print("WARNING: You'll be waiting for long time for enumerating too many possibilities!")

    J = 0.0
    for k in range(1, 2**size+1):
        design = utility.index_to_binary_state(k, size=size)
        try:
            v = objective_value_tracker[k]
        except(TypeError, KeyError):
            v = func(design)

        _, p = pmf(design, theta)
        # print(w, v, p)
        J += v * p

    return J

def pmf(x, theta):
    """
    Calculate the value of the probability mass function (PMF) of
    a uncorrelated multivariate Bernoulli distribution, evaluated at given binary state x,
    and with parameters theta.

    Args:
        x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        theta: scalar, or 1D numpy array; all entries are in the interval [0, 1]

    Returns:
        marginal_prob: the marginal probability of each entry
        joint_prob: the joint probability of entries of x

    """
    # Reshaping and assertion
    x = np.array(x, dtype=bool).flatten().astype(int)
    theta = np.array(theta, dtype=np.float).flatten()
    size = max(x.size, theta.size)
    if 1==x.size<size: x = np.ones(size, dtype=int)*x[0]
    if 1==theta.size<size: theta = np.ones(size)*theta[0]
    if any(theta<0) or any(theta>1):
        print("Theta Must be in the interval [0, 1] for all entries!")
        raise ValueError
    assert x.size == theta.size, 'size mismatch!'

    # Calculate probabilities (marginal and joint)
    v0 = np.power(theta, x)
    v1 = np.power(1-theta, 1-x)
    marginal_prob = v0 * v1
    marginal_prob[np.isinf(marginal_prob)] = np.nan
    joint_prob = np.product(marginal_prob)
    return marginal_prob, joint_prob

def log_pmf(x, theta):
    """
    Calculate the value of log-PMF (probability mass function) of
    a uncorrelated multivariate Bernoulli distribution

    Args:
        x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        theta: scalar, or 1D numpy array; all entries are in the interval [0, 1]

    Returns:
        marginal_log_prob: the marginal log-probability of each entry
        joint_log_prob: the joint log-probability of entries of x

    """
    # Retrieve PMF values (marginal probabilites), and take log
    marginal_prob, _ = pmf(x, theta)
    marginal_log_prob = np.log(marginal_prob)
    marginal_log_prob[np.isinf(marginal_log_prob)] = np.nan
    joint_log_prob = np.sum(marginal_log_prob)
    return marginal_log_prob, joint_log_prob

def grad_pmf(x, theta):
    """
    Calculate the gradient of the probability mass function (PMF) of
    a uncorrelated multivariate Bernoulli distribution, evaluated at given binary state x,
    and with parameters theta.

    Args:
        x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        theta: scalar, or 1D numpy array; all entries are in the interval [0, 1]

    Returns:
        grad_marginal_prob: the marginal probability of each entry
        grad_joint_prob: the joint probability of entries of x

    """
    # Reshaping and assertion
    x = np.array(x, dtype=bool).flatten().astype(int)
    theta = np.array(theta).flatten()
    size = max(x.size, theta.size)
    if 1==x.size<size: x = np.ones(size, dtype=int)*x[0]
    if 1==theta.size<size: theta = np.ones(size)*theta[0]
    if any(theta<0) or any(theta>1):
        print("Theta Must be in the interval [0, 1] for all entries!")
        raise ValueError
    assert x.size == theta.size, 'size mismatch!'

    # Calculate probabilities (marginal and joint)
    marginal_prob, joint_prob = pmf(x, theta)
    scaling = np.array([np.prod(marginal_prob[:i]) * np.prod(marginal_prob[i+1:]) for i in range(size)])

    # Calculate gradient
    grad_marginal_prob = np.power(-np.ones(size), 1-x)
    grad_joint_prob = scaling * grad_marginal_prob
    return grad_marginal_prob, grad_joint_prob

def _grad_log_pmf(x, theta):
    """
    Calculate the gradient of log-PMF (probability mass function), w.r.t. parameters theta,
    of a uncorrelated multivariate Bernoulli distribution

    Args:
        x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        theta: scalar, or 1D numpy array; all entries are in the interval [0, 1]

    Returns:
        grad_marginal_prob: the marginal probability of each entry
        grad_joint_prob: the joint probability of entries of x

    Remarks:

        Gradient of log-probabilites is same as partial derivatives of corresponding derivatives of log-prob of each entry
        This is the the original version of 'grad_log_pmf'; once the new version is updated, this will be removed

    """
    # Calculate Derivative of Log-probabilities (marginal & joint) w.r.t the parameter (theta)
    marginal_pmf, _ = pmf(x, theta)
    marginal_grad_pmf, _ = grad_pmf(x, theta)

    grad_marginal_log_prob = marginal_grad_pmf / marginal_pmf
    grad_marginal_log_prob[np.isinf(grad_marginal_log_prob)] = np.nan
    # print("grad_log_pmf: ", x, theta, marginal_pmf, marginal_grad_pmf, grad_marginal_log_prob)
    return grad_marginal_log_prob, grad_marginal_log_prob.copy()



def grad_log_pmf(x, theta, zero_bounds=True):
    """
    Calculate the gradient of log-PMF (probability mass function), w.r.t. parameters theta,
    of a uncorrelated multivariate Bernoulli distribution

    Args:
        x: scalar, or 1D numpy array, or binary values (0/10) or bytes
        theta: scalar, or 1D numpy array; all entries are in the interval [0, 1]
        zero_bounds: This

    Returns:
        grad_marginal_prob: the marginal probability of each entry
        grad_joint_prob: the joint probability of entries of x

    Remarks:
        Gradient of log-probabilites is same as partial derivatives of corresponding derivatives of log-prob of each entry
        zero_bounds is the new argument that will become permanent in later versions. It signifies the need to single-out any entries with zero or 1 probability

    """
    # Calculate Derivative of Log-probabilities (marginal & joint) w.r.t the parameter (theta)
    x = np.asarray(x).flatten()
    marginal_pmf, joint_pmf = pmf(x, theta)
    marginal_grad_pmf, joint_grad_pmf = grad_pmf(x, theta)
    dimension = np.size(theta)

    if not zero_bounds:
        grad_marginal_log_prob = marginal_grad_pmf / marginal_pmf
        grad_marginal_log_prob[np.isinf(grad_marginal_log_prob)] = np.nan
        grad_joint_log_prob = grad_marginal_log_prob.copy()
    else:
        # This branch will be the default later
        # 1- Find entries with zero probabilites, and assign nan values
        # 2- Find entries with 1 probabilities, and assign 0
        # 3- Call the function with projected entries to calculate proper grad-log-pmf
        # Augment values
        zeros_entries = np.where(marginal_pmf==0)[0]
        ones_entries  = np.where(marginal_pmf==1)[0]
        bound_entries = np.union1d(zeros_entries, ones_entries)
        valid_entries = np.setdiff1d(np.arange(dimension), bound_entries)

        # print("Theta: ", theta)
        # print("Bound Entries: ", bound_entries)
        # print("Valid Entries: ", valid_entries)

        grad_marginal_log_prob = np.empty(dimension)
        grad_marginal_log_prob[zeros_entries] = np.nan
        grad_marginal_log_prob[ones_entries]  = 0.0

        grad_marginal_log_prob[valid_entries] = x[valid_entries]/theta[valid_entries] - (1-x[valid_entries])/(1-theta[valid_entries])

        # Joint and marginal gradients of log probabilities coincide for uncorrelated multivariate Bernoulli PMF
        grad_joint_log_prob = grad_marginal_log_prob.copy()

    return grad_marginal_log_prob, grad_joint_log_prob


