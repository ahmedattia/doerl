

"""
This module shows how to carry out binary OED with 1d toy model.
"""

import sys, os
doerl_path = os.path.abspath('../../')
if not doerl_path in sys.path: sys.path.append(doerl_path)

import numpy as np
import pickle
import matplotlib.pyplot as plt

import doerl
from doerl.models.linear import tomography
from doerl.utility import utility
from doerl.optimization import optimization
from doerl.stats import bernoulli as s_bernoulli


def a_optimality_objective(F, B, R, design, penalty_parameter=0, penalty_norm=0, objective_value_tracker=None, budget=None):
    """
    Calculate  A-optimality criterion (Trace of posterior covariance matrix)
    modified version of utility.a_optimality_objective

    Args:
        F
        B
        R
        design
        penalty_parameter
        penalty_norm

    Returns:
        objective = trace of the posterir covariance matrix (by applying Kalman update formula) + norm
    """
    design = np.asarray(design, dtype=bool)
    obs_n_angles = design.size
    image_n_voxels = np.size(F, 0) // obs_n_angles

    active_indexes, _ = tomography.get_active_indexes(image_n_voxels=image_n_voxels,
                                                      obs_n_angles=obs_n_angles,
                                                      design=design)

    if objective_value_tracker is None: objective_value_tracker = dict()
    k = utility.index_from_binary_state(design)

    if k in objective_value_tracker.keys():
        trc = objective_value_tracker[k]

    elif active_indexes.size == 0:
        trc = utility.a_optimality_objective(F=None,
                                             B=B,
                                             R=None,
                                             design=None,
                                             penalty_parameter=0,
                                             penalty_norm=penalty_norm)
    else:
        trc = utility.a_optimality_objective(F=F[active_indexes, :],
                                             B=B,
                                             R=R[active_indexes, :][:, active_indexes],
                                             design=np.ones_like(active_indexes, dtype=bool),
                                             penalty_parameter=0,
                                             penalty_norm=penalty_norm)

    # add penalty
    if budget is not None:
        violation = np.linalg.norm(np.array(design).flatten(), ord=0) - budget
        val = trc + penalty_parameter * abs(violation)
    else:
        val = trc + penalty_parameter * np.linalg.norm(np.array(design).flatten(), ord=penalty_norm)

    return val


def test_tomography_problem(F, B, R,
                            obs_n_angles,
                            batch_size=32,
                            epochs=1,
                            baseline=None,
                            penalty_parameter=0.0,
                            penalty_norm=0,
                            budget=None,
                            learning_rate=1.0/4.0,
                            exclude_bounds=False,
                            antithetic=True,
                            opt_max_iter=50,
                            bruteforce_search=False,
                            objective_value_tracker=None,
                            output_dir='PLOTS/Tomography/',
                            plots_format='pdf',
                            random_seed=123,
                            verbose=False):
    """
    Test tomography problem

    Args:
        F: Forwad operator 2 x n array-like (numpy.ndarray or scipy.sparse.csr_matrix)
        B: Prior Covariances: n x n matrix
        R: Observation covariances: 2/2 array
        batch_size:
        baseline: None -->0, 'empirical', 'optimal'
        penalty_parameter:
        learning_rate: default optimization step-size
        exclude_bounds: Ignore 0, 1 from values of the hyperparameter theta
        antithetic:
        num_test_points: spacing points in each direction to test ; for comparison and plotting
        opt_max_iter: maximum number of iterations the optimizer can take
        output_dir: path to save results/plots to; if None; CWD is used
        plots_format: matplotlib figure format (pdf, png, ...)
        random_seed: use only for reproducibility
        verbose:

    """
    if random_seed is not None: np.random.seed(random_seed)

    # Prepare output directory
    if output_dir is None:
        output_dir = os.getcwd()
    if not os.path.isdir(output_dir): os.makedirs(output_dir)

    # Solution of the inverse problem (with al observation angles)
    print("Solving the inverse problem: All observation angles are ON...")
    n_voxels = int(np.sqrt(np.size(F, 1)))
    true_image = tomography.get_shepp_logan_phantom_example(n_voxels=n_voxels)
    prior_image = true_image + np.linalg.cholesky(B).dot(np.random.randn(true_image.size))
    data = utility.foward_problem(F, true_image, R=R)

    # Solve Linear inverse problem with all angles on
    post_mean, post_cov = utility.kalman_filter(xb=prior_image, y=data, F=F, B=B, R=R)

    # OED:
    # Check baseline, and define objective (lambda just for better readibility)
    obj = lambda w: a_optimality_objective(F=F,
                                           B=B,
                                           R=R,
                                           design=w,
                                           penalty_parameter=penalty_parameter,
                                           objective_value_tracker=objective_value_tracker,
                                           penalty_norm=penalty_norm,
                                           budget=budget)

    # Search by enumeration!
    if bruteforce_search:
        print("Solution by Enumeration: Brute-Force...")
        if objective_value_tracker is None: objective_value_tracker = dict()
        num_bruteforce_candidates = 2**obs_n_angles
        winner_obj = np.infty
        for k in range(num_bruteforce_candidates):
            try:
                val = objective_value_tracker[k+1]
                design = utility.index_to_binary_state(k+1, size=obs_n_angles)
            except(KeyError):
                design = utility.index_to_binary_state(k+1, size=obs_n_angles)
                val = obj(design)
                objective_value_tracker.update({k+1:val})
            if val < winner_obj:
                winner_design = design.copy()
                winner_k = k+1
                winner_obj = val
            print("\rPossible Design: [%d/%d]; Objective= %12.4f " %(k+1,
                                                                     num_bruteforce_candidates,
                                                                     val), end='')
        print("\nWinner design index: %d; winner objective value: %f "  %(winner_k, winner_obj))
        print("Winner Design: %s" % repr(winner_design))

        # Dump BruteForce;
        results_file = os.path.join(output_dir, "BruteForceResults.pickle")
        pickle.dump(objective_value_tracker, open(results_file, 'wb'))
        print("Results pickled to: %s" % results_file)

    else:
        if objective_value_tracker is None: objective_value_tracker = dict()

    # Solve the stochastic OED problem to find best set of angles
    observation_dimension = data.size
    init_theta = np.ones(obs_n_angles) * 0.5

    # OptimalBaseline
    print("*** Stochastic Binary Optimization *** ")
    theta, _, _, traject, converged, msg, objective_value_tracker, \
        sampled_design_indexes = optimization.binary_REINFORCE(J=obj,
                                                               init_theta=init_theta,
                                                               def_stepsize=learning_rate,
                                                               batch_size=batch_size,
                                                               max_iter=opt_max_iter,
                                                               pgtol=1e-8,
                                                               baseline=baseline,
                                                               monitor_iterations=True,
                                                               antithetic=antithetic,
                                                               objective_value_tracker=objective_value_tracker,
                                                               verbose=verbose)

    print("Optimal policy: %s" % repr(theta))
    optimal_policy_sample = s_bernoulli.sample(theta, batch_size, antithetic=antithetic)
    optimal_policy_sample_objective = [obj(design) for design in optimal_policy_sample]
    print("Optimal Plicy Sample:", optimal_policy_sample)
    print("Objective values of the sample: ", optimal_policy_sample_objective)

    # Create a dictionary, save results, and return after plotting
    results = dict(
        # Book Keep Results
        n_voxels=n_voxels,
        obs_n_angles=obs_n_angles,
        # Forward/Inverse problem elements
        F=F,
        B=B,
        R=R,
        true_image=true_image,
        prior_image=prior_image,
        data=data,
        posterior_image=post_mean,
        posterior_covariace=post_cov,
        # reinforce (binary optimization)
        batch_size=batch_size,
        epochs=epochs,
        baseline=baseline,
        penalty_parameter=penalty_parameter,
        penalty_norm=penalty_norm,
        learning_rate=learning_rate,
        initial_policy=init_theta,
        optimal_policy=theta,
        reinforce_traject=traject,
        reinforce_converged=converged,
        reinforce_msg=msg,
        reinforce_objective_value_tracker=objective_value_tracker,
        reinforce_sampled_design_indexes=sampled_design_indexes,
        reinforce_optimal_policy_sample=optimal_policy_sample,
        reinforce_optimal_policy_sample_objective=optimal_policy_sample_objective
    )

    # Dump Results;
    results_file = os.path.join(output_dir, "Tomography_Model_Results.pickle")
    pickle.dump(results, open(results_file, 'wb'))
    print("Results pickled to: %s" % results_file)

    # Plot Results:
    print("Plotting results: TODO")



if __name__ == '__main__':

    # Settings
    image_n_voxels                = 100  # Full: 100; testing: 50
    obs_n_angles                  = 5  # Full: 30 ; testing: 10; bruteforce: 15
    obs_angles                    = None
    scale_radon_matrix            = True
    obs_noise_level               = 0.10
    prior_stdev                   = 0.1
    prior_covariances             = False
    observation_stdev             = 0.015
    observation_error_covariances = False
    allow_sparse                  = False
    verbose                       = True

    bruteforce_search             = True

    # Regularization (update if you want to impose regularization)
    penalty_norm      = 0    # 0 --> \ell_0
    penalty_parameter = 1
    budget            = 2    # Number of angles to look for
    baseline          = None
    # Set dimensions
    F, B, R = tomography.create_inverse_problem_elements(image_n_voxels=image_n_voxels,
                                                         obs_n_angles=obs_n_angles,
                                                         obs_theta=obs_angles,
                                                         scale_radon_matrix=scale_radon_matrix,
                                                         obs_noise_level=obs_noise_level,
                                                         prior_stdev=prior_stdev,
                                                         prior_covariances=prior_covariances,
                                                         observation_stdev=observation_stdev,
                                                         observation_error_covariances=observation_error_covariances,
                                                         allow_sparse=allow_sparse,
                                                         verbose=verbose)

    try:
        bruteforce_dict = pickle.load(open('PLOTS/Tomography/BruteForceResults.pickle', 'rb'))
    except:
        bruteforce_dict = None

    # Test and Plot 1d Toy Problem (See the function arguments if you want to play with it)
    test_tomography_problem(F=F,
                            B=B,
                            R=R,
                            obs_n_angles=obs_n_angles,
                            baseline=baseline,
                            budget=budget,
                            bruteforce_search=bruteforce_search,
                            penalty_norm= penalty_norm,
                            penalty_parameter=penalty_parameter,
                            objective_value_tracker=bruteforce_dict,
                            verbose=verbose)


