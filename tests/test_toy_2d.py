

"""
This module shows how to carry out binary OED with 1d toy model.
"""

import sys, os
doerl_path = os.path.abspath('../../')
if not doerl_path in sys.path: sys.path.append(doerl_path)

import numpy as np
import pickle

from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

import doerl
from doerl.utility import utility
from doerl.optimization import optimization
from doerl.stats import bernoulli
from doerl.models.linear import toy_linear


# Generate Linear Model (Trivial matrices) setup, and build prior-> posterior covariances
# Use these to test OED criterion values, behaviour, and optimization


def test_2d_design_problem(F,
                           B,
                           R,
                           batch_size=32,
                           epochs=10,
                           baseline=None,
                           penalty_parameter=0.0,
                           penalty_norm=0,
                           learning_rate=1.0/4.0,
                           exclude_bounds=False,
                           antithetic=True,
                           num_test_points=15,  # 20 is fair enough
                           opt_max_iter=50,
                           output_dir='PLOTS/TwoD_Toy/',
                           plots_format='pdf',
                           random_seed=123,
                           verbose=False):
    """
    Assume 2d model, and one candidate sensor location, test the
    OED problem using relaxed and Stochastic-Binary formulation,
    and create suitable plots

    Args:
        F: Forwad operator 2 x n array-like (numpy.ndarray or scipy.sparse.csr_matrix)
        B: Prior Covariances: n x n matrix
        R: Observation covariances: 2/2 array
        batch_size:
        baseline: None -->0, 'empirical', 'optimal'
        penalty_parameter:
        learning_rate: default optimization step-size
        exclude_bounds: Ignore 0, 1 from values of the hyperparameter theta
        antithetic:
        num_test_points: spacing points in each direction to test ; for comparison and plotting
        opt_max_iter: maximum number of iterations the optimizer can take
        output_dir: path to save results/plots to; if None; CWD is used
        plots_format: matplotlib figure format (pdf, png, ...)
        random_seed: use only for reproducibility
        verbose:

    """
    assert np.ndim(F) == 2, "F must be 2d array like with shape (1, *), i.e., one row"
    observation_dimension, model_size = F.shape
    if observation_dimension != 2:
        print("This is tailored for 2d observation space only!")
        raise TypeError
    assert B.shape == (model_size, model_size), "B must be  2d array-like with shape (%d, %d)" % (model_size, model_size)
    assert R.shape == (observation_dimension, observation_dimension), "R must be 2d array-like with shape (%d, %d)" % (observation_dimension, observation_dimension)

    if random_seed is not None:
        np.random.seed(random_seed)
    #
    # Check baseline, and define objective (lambda just for better readibility)
    obj = lambda w: utility.a_optimality_objective(F=F,
                                           B=B,
                                           R=R,
                                           design=w,
                                           penalty_parameter=penalty_parameter,
                                           penalty_norm=penalty_norm)

    # ****
    # 1- Brute-Force solution; lookup all possible binary designs
    # ****

    # Evaluate objective for all possible bruteforce solutions; here just design is either 0 or 1
    bruteforce_candidates = utility.enumerate_binary(observation_dimension)
    num_bruteforce_candidates = len(bruteforce_candidates)
    bruteforce_objective_values = np.empty(num_bruteforce_candidates)
    print("Evaluating Objective function for all possible candidate binary designs")
    for i, w in enumerate(bruteforce_candidates):
        bruteforce_objective_values[i] = obj(w)
        if verbose:
            print("Possible Design: [%d/%d]; Objective= %12.4f " %(i+1,
                                                                   num_bruteforce_candidates,
                                                                   bruteforce_objective_values[i]))
        else:
            print("\rPossible Design: [%d/%d]; Objective= %12.4f " %(i+1,
                                                                     num_bruteforce_candidates,
                                                                     bruteforce_objective_values[i]), end='')
    print()

    # Find the design with smallest value of the objective
    # This is complicated only to be used with 2d problems
    winner_ind = tr_argsorter = np.argsort(bruteforce_objective_values)[0]
    bruteforce_winner_design = bruteforce_candidates[winner_ind]
    bruteforce_winner_design_objective = bruteforce_objective_values[winner_ind]
    print("\n**\nWinner Design: %s\nMinimum Trace: %f\n**" %(bruteforce_winner_design,
                                                                 bruteforce_winner_design_objective))

    # ****
    # 2- Relaxed-OED; Evaluate the objective function for multiple values of the relaxed designs
    # ****

    # Relaxed Design (Standard OED)
    num_w = num_test_points
    design_weights = np.linspace(0, 1, num_w)
    relaxed_design_candidates = np.empty((num_w, num_w, observation_dimension))
    relaxed_design_candidates[..., 0], relaxed_design_candidates[..., 1] = np.meshgrid(design_weights, design_weights)
    relaxed_design_objective_values = np.empty((num_w, num_w))
    num_relaxed_design_candidates = num_w ** 2
    for i in range(num_w):
        for j in range(num_w):
            w = np.array([relaxed_design_candidates[i, j, 0], relaxed_design_candidates[i, j, 1]])
            relaxed_design_objective_values[i, j] = obj(w)
            if verbose:
                print("Candidate Relaxed design [%d/%d]; Objective: %12.4f " % (i*num_w+(j+1),
                    num_relaxed_design_candidates, relaxed_design_objective_values[i, j]))
            else:
                print("\rCandidate Relaxed design [%d/%d]; Objective: %12.4f " % (i*num_w+(j+1),
                    num_relaxed_design_candidates, relaxed_design_objective_values[i, j]), end='')
    print()

    # ****
    # 3- Stochastic-Binary OED; The proposed approach
    #    Apply without baseline; with empirical baseline, and with optimal baseline
    # ****
    # i  ) Evaluate the objective value for several possible values of the hyperparam theta in [0,1]
    # ii ) Evaluate the exact gradient at all possible values of the hyperparam theta
    # iii) Evaluate the stochastic gradient at all possible values of the hyperparameter
    #      (theta) for each case of baseline above, i.e., None, 'empirical', 'optimal'
    #
    # Possible values of the hyperparameter theta (probability of activating design(s) ),
    # and the corresponding value of the stochastic objective function E(J(design|theta))
    # ****

    # 3.i  )
    # TODO: For 2D, replace with a 2d mesh
    num_thetas = num_test_points
    thetas = np.linspace(0, 1, num_thetas, endpoint=True)
    if exclude_bounds:
        thetas = thetas[1: -1]
        num_thetas -= 2
    num_hyperparam_candidates = num_thetas ** 2

    hyperparameter_candidates = np.empty((num_thetas, num_thetas, observation_dimension))
    hyperparameter_candidates[..., 0], hyperparameter_candidates[..., 1] = np.meshgrid(thetas, thetas)
    hyperparameter_candidates_objective_values = np.empty((num_thetas, num_thetas))
    print("Evaluating Stochastic-Objective function for multiple values of the hyperparameter")
    for i in range(num_thetas):
        for j in range(num_thetas):
            theta = np.array([hyperparameter_candidates[i, j, 0], hyperparameter_candidates[i, j, 1]])
            hyperparameter_candidates_objective_values[i, j] = bernoulli.expect(theta, obj)

            if verbose:
                print("Candidate Hyperparameter [%d/%d]; Stochastic-Objective: %12.4f " % (i*num_w+(j+1),
                     num_hyperparam_candidates, hyperparameter_candidates_objective_values[i, j]))
            else:
                print("\rCandidate Hyperparameter [%d/%d]; Stochastic-Objective: %12.4f " % (i*num_w+(j+1),
                     num_hyperparam_candidates, hyperparameter_candidates_objective_values[i, j]), end='')
    print()

    winner_ind = np.unravel_index(np.argsort(hyperparameter_candidates_objective_values, axis=None)[0],
                                  hyperparameter_candidates_objective_values.shape)
    winner_hyperparameter = np.array([hyperparameter_candidates[winner_ind[0], winner_ind[1], 0],
                                      hyperparameter_candidates[winner_ind[0], winner_ind[1], 1]])
    winner_hyperparameter_objective = hyperparameter_candidates_objective_values[winner_ind]
    print("\n**\nWinner Hyperparameter: %s\nMinimum Objective: %f\n**" %(winner_hyperparameter,
                                                             winner_hyperparameter_objective))

    # 3.ii )
    # Gradient at multiple values of the hyperparameter; the full-expectation
    exact_policy_gradients = np.empty((num_thetas, num_thetas, observation_dimension))

    print("Evaluating exact policy gradient multiple values of the hyperparameter")
    for i in range(num_thetas):
        for j in range(num_thetas):
            if verbose:
                print("Candidate Hyperparameter [%d/%d] " % (i*num_w+(j+1), num_hyperparam_candidates))
            else:
                print("\rCandidate Hyperparameter [%d/%d] " % (i*num_w+(j+1), num_hyperparam_candidates), end='')
            theta = np.array([hyperparameter_candidates[i, j, 0], hyperparameter_candidates[i, j, 1]])
            exact_policy_gradients[i, j, :] = optimization.exact_policy_gradient(J=obj,
                                                                                 theta=theta,
                                                                                 apply_projection=False,
                                                                                 verbose=verbose)
    print()



    # 3.ii ) Stochastic approximation of the gradient
    stochastic_policy_gradients_no_baseline = np.empty((num_thetas, num_thetas, observation_dimension))
    print("Evaluating sochastic policy gradient multiple values of the hyperparameter")
    print("** No Baseline **")
    for i in range(num_thetas):
        for j in range(num_thetas):
            theta = np.array([hyperparameter_candidates[i, j, 0], hyperparameter_candidates[i, j, 1]])
            if verbose:
                print("Candidate Hyperparameter [%d/%d] " % (i*num_w+(j+1), num_hyperparam_candidates))
            else:
                print("\rCandidate Hyperparameter [%d/%d] " % (i*num_w+(j+1), num_hyperparam_candidates), end='')
            stochastic_policy_gradients_no_baseline[i, j, :] = optimization.stochastic_policy_gradient(J=obj,
                                                                                                       theta=theta,
                                                                                                       batch_size=batch_size,
                                                                                                       apply_projection=False,
                                                                                                       antithetic=antithetic,
                                                                                                       verbose=verbose)
    print()

    # calcaulate empirical baseline (constant)
    stochastic_policy_gradients_empirical_baseline = np.empty((num_thetas, num_thetas, observation_dimension))
    empirical_baseline = (obj(0) + obj(1)) / 2.0
    baseline_objective = lambda w: obj(w) - empirical_baseline
    print("Evaluating sochastic policy gradient multiple values of the hyperparameter")
    print("** Huristic/Empirical Baseline **")
    print("\t baseline = %f" % empirical_baseline)
    for i in range(num_thetas):
        for j in range(num_thetas):
            if verbose:
                print("Candidate Hyperparameter [%d/%d] " % (i*num_w+(j+1), num_hyperparam_candidates))
            else:
                print("\rCandidate Hyperparameter [%d/%d] " % (i*num_w+(j+1), num_hyperparam_candidates), end='')
            theta = np.array([hyperparameter_candidates[i, j, 0], hyperparameter_candidates[i, j, 1]])
            stochastic_policy_gradients_empirical_baseline[i, j, :] = optimization.stochastic_policy_gradient(J=baseline_objective,
                                                                                                              theta=theta,
                                                                                                              batch_size=batch_size,
                                                                                                              apply_projection=False,
                                                                                                              antithetic=antithetic,
                                                                                                              verbose=verbose)
    print()

    # calcaulate optimal baseline (for each theta)
    stochastic_policy_gradients_optimal_baseline = np.empty((num_thetas, num_thetas, observation_dimension))
    print("Evaluating sochastic policy gradient multiple values of the hyperparameter")
    print("** Optimal Baseline **")
    for i in range(num_thetas):
        for j in range(num_thetas):
            theta = np.array([hyperparameter_candidates[i, j, 0], hyperparameter_candidates[i, j, 1]])
            optimal_baseline = optimization.optimal_baseline_estimate(J=obj,
                                                                      theta=theta,
                                                                      batch_size=batch_size,
                                                                      epochs=epochs,
                                                                      antithetic=antithetic,
                                                                      verbose=verbose
                                                                      )
            if verbose:
                print("Candidate Hyperparameter [%d/%d]; Optimal Baseline Estimate=%8.6f" % (i*num_w+(j+1), num_hyperparam_candidates, optimal_baseline))
            else:
                print("\rCandidate Hyperparameter [%d/%d]; Optimal Baseline Estimate=%8.6f" % (i*num_w+(j+1), num_hyperparam_candidates, optimal_baseline), end='')
            #
            baseline_objective = lambda w: obj(w) - optimal_baseline
            stochastic_policy_gradients_optimal_baseline[i, j, :] = optimization.stochastic_policy_gradient(J=baseline_objective,
                                                                                                         theta=theta,
                                                                                                         batch_size=batch_size,
                                                                                                         apply_projection=False,
                                                                                                         antithetic=antithetic,
                                                                                                         verbose=verbose)
    print()

    # Test the stochastic-binary OED (REINFORCE) Algorithm
    init_theta = np.ones(observation_dimension) * 0.5
    # No-Baseline
    print("*** Stochastic Binary Optimization; No Baseline *** ")
    _, _, _, traject, _, _, _, _ = optimization.binary_REINFORCE(J=obj,
                                                        init_theta=init_theta,
                                                        def_stepsize=learning_rate,
                                                        batch_size=batch_size,
                                                        max_iter=opt_max_iter,
                                                        pgtol=1e-8,
                                                        baseline=None,
                                                        monitor_iterations=True,
                                                        antithetic=antithetic,
                                                        verbose=verbose)
    no_baseline_optimization_trajectory = np.array(traject)
    no_baseline_optimization_trajectory_objectives = np.array([bernoulli.expect(x, obj) for x in traject])

    # Empirical/Huristic-Baseline
    print("*** Stochastic Binary Optimization; Empirical Baseline *** ")
    _, _, _, traject, _, _, _, _ = optimization.binary_REINFORCE(J=obj,
                                                        init_theta=init_theta,
                                                        def_stepsize=learning_rate,
                                                        batch_size=batch_size,
                                                        max_iter=opt_max_iter,
                                                        pgtol=1e-8,
                                                        baseline='empirical',
                                                        monitor_iterations=True,
                                                        antithetic=antithetic,
                                                        verbose=verbose)
    empirical_baseline_optimization_trajectory = np.array(traject)
    empirical_baseline_optimization_trajectory_objectives = np.array([bernoulli.expect(x, obj) for x in traject])

    # OptimalBaseline
    print("*** Stochastic Binary Optimization; Optimal Baseline *** ")
    _, _, _, traject, _, _, _, _ = optimization.binary_REINFORCE(J=obj,
                                                        init_theta=init_theta,
                                                        def_stepsize=learning_rate,
                                                        batch_size=batch_size,
                                                        max_iter=opt_max_iter,
                                                        pgtol=1e-8,
                                                        baseline='optimal',
                                                        monitor_iterations=True,
                                                        antithetic=antithetic,
                                                        verbose=verbose)
    optimal_baseline_optimization_trajectory = np.array(traject)
    optimal_baseline_optimization_trajectory_objectives = np.array([bernoulli.expect(x, obj) for x in traject])

    # Create a dictionary, save results, and return after plotting
    #
    # Prepare output directory
    if output_dir is None:
        output_dir = os.getcwd()
    if not os.path.isdir(output_dir): os.makedirs(output_dir)
    #
    results = dict(
        # Book Keep Results
        F=F,
        B=B,
        R=R,
        # Bruteforce for Designs
        bruteforce_candidates=bruteforce_candidates,
        bruteforce_objective_values=bruteforce_objective_values,
        bruteforce_winner_design=bruteforce_winner_design,
        bruteforce_winner_design_objective=bruteforce_winner_design_objective,
        #
        # Relaxed Design
        relaxed_design_candidates=relaxed_design_candidates,
        relaxed_design_objective_values=relaxed_design_objective_values,
        #
        # Bruteforce for Hyperparameters
        hyperparameter_candidates=hyperparameter_candidates,
        hyperparameter_candidates_objective_values=hyperparameter_candidates_objective_values,
        winner_hyperparameter=winner_hyperparameter,
        winner_hyperparameter_objective=winner_hyperparameter_objective,
        #
        # Gradient evaluation/estimation results
        exact_policy_gradients=exact_policy_gradients,
        stochastic_policy_gradients_no_baseline=stochastic_policy_gradients_no_baseline,
        stochastic_policy_gradients_empirical_baseline=stochastic_policy_gradients_empirical_baseline,
        stochastic_policy_gradients_optimal_baseline=stochastic_policy_gradients_optimal_baseline,
        #
        # Stochastic-Optimization Results
        no_baseline_optimization_trajectory=no_baseline_optimization_trajectory,
        no_baseline_optimization_trajectory_objectives=no_baseline_optimization_trajectory_objectives,
        empirical_baseline_optimization_trajectory=empirical_baseline_optimization_trajectory,
        empirical_baseline_optimization_trajectory_objectives=empirical_baseline_optimization_trajectory_objectives,
        optimal_baseline_optimization_trajectory=optimal_baseline_optimization_trajectory,
        optimal_baseline_optimization_trajectory_objectives=optimal_baseline_optimization_trajectory_objectives
    )

    # Dump Results;
    results_file = os.path.join(output_dir, "TwoD_ToyModel_Results.pickle")
    pickle.dump(results, open(results_file, 'wb'))
    print("Results pickled to: %s" % results_file)

    #
    # 1- Plots Objective functions (Exact with relaxation and stochastic)
    utility.plots_enhancer(fontsize=20, usetex=True)
    # *- Add optimal design values and hyperparameters to the plot above
    fig = plt.figure(figsize=(18, 10))
    ax = fig.add_subplot(111, projection='3d')
    relaxed_design_candidates = np.array(relaxed_design_candidates)
    surf1 = ax.plot_surface(relaxed_design_candidates[..., 0],
                            relaxed_design_candidates[..., 1],
                            relaxed_design_objective_values,
                            alpha=0.9,
                            zorder=1,
                            label=r"Relaxed objective $\mathcal{J}(\mathbf{\xi})$"
                            )

    hyperparameter_candidates = np.array(hyperparameter_candidates)
    surf2 = ax.plot_surface(hyperparameter_candidates[..., 0],
                            hyperparameter_candidates[..., 1],
                            hyperparameter_candidates_objective_values,
                            alpha=0.7,
                            zorder=10,
                            label=r"Stochastic objective $\Upsilon(\mathbf{\theta)}$")


    # Mark winners
    ax.scatter3D([bruteforce_winner_design[0]],
                 [bruteforce_winner_design[1]],
                 [winner_hyperparameter_objective],
                 marker='o',
                 facecolor=(0,0,0,0),
                 edgecolor='k',
                 linewidths=2,
                 s=20**2,
                 label=r"Bruteforce $\mathbf{\xi}^{\rm opt}$",
                 )
    ax.scatter3D([winner_hyperparameter[0]],
                 [winner_hyperparameter[1]],
                 [winner_hyperparameter_objective],
                 marker='*',
                 edgecolor='k',
                 linewidths=2,
                 s=18**2,
                 facecolor=(0,0,0,0),
                 label=r"Bruteforce $\mathbf{\theta}^{\rm opt}$",
                 )

    # Ticks & Labels
    ax.set_xlabel(r'$\theta_1 / \xi_1$', labelpad=10)
    ax.set_ylabel(r'$\theta_2 / \xi_2$', labelpad=10)
    ax.set_zlabel(r'Objective value $\mathcal{J}(\mathbf{\xi})\, /\, \Upsilon(\mathbf{\theta})$ ', labelpad=10)
    for surf in [surf1, surf2]:
        try:
            surf._facecolors2d=surf._facecolors3d
            surf._edgecolors2d=surf._edgecolors3d
        except(AttributeError):
            surf._facecolors2d=surf._facecolor3d
            surf._edgecolors2d=surf._edgecolor3d
    ax.legend(loc='center right', bbox_to_anchor=(0.0, 0.5), framealpha=0.85, borderaxespad=0.)

    # Save Plot
    plot_file = os.path.join(output_dir, "TwoD_ToyModel_Objective_Values.%s" % plots_format.strip('. '))
    fig.savefig(plot_file, dpi=600, bbox_inches='tight')
    print("Saved plot: %s" % plot_file)

    # Add trajectories and resave, then close
    line_colors = ['#1f77b4', '#2ca02c', '#d62728']
    linewidth = 3
    markersize = linewidth*3
    xs = no_baseline_optimization_trajectory[:, 0]
    ys = no_baseline_optimization_trajectory[:, 1]
    zs = no_baseline_optimization_trajectory_objectives
    ax.plot(xs, ys, zs, '-o', color=line_colors[0], markersize=markersize, linewidth=linewidth, label=r"Optimization: no baseline", zorder=100)

    xs = empirical_baseline_optimization_trajectory[:, 0]
    ys = empirical_baseline_optimization_trajectory[:, 1]
    zs = empirical_baseline_optimization_trajectory_objectives
    ax.plot(xs, ys, zs, '--d', color=line_colors[1], markersize=markersize, linewidth=linewidth, label=r"Optimization: empirical baseline", zorder=110)

    xs = optimal_baseline_optimization_trajectory[:, 0]
    ys = optimal_baseline_optimization_trajectory[:, 1]
    zs = optimal_baseline_optimization_trajectory_objectives
    ax.plot(xs, ys, zs, '-.*', color=line_colors[2], markersize=markersize, linewidth=linewidth, label=r"Optimization: optimal baseline", zorder=120)
    ax.legend(loc='center right', bbox_to_anchor=(0.0, 0.5), framealpha=0.85, borderaxespad=0.)

    # Save Plot
    plot_file = os.path.join(output_dir, "TwoD_ToyModel_Objective_Values_with_Optimization.%s" % plots_format.strip('. '))
    fig.savefig(plot_file, dpi=600, bbox_inches='tight')
    print("Saved plot: %s" % plot_file)
    plt.close(fig)

    # 2- Plot All gradients given the values of the hyperparameter on one plot
    fig, axes = plt.subplots(2, 2, figsize=(12, 12), sharex='all', sharey='all')
    quiver_linewidth = 0.5
    # mask edges (TODO: Utilize the fixed at-bound gradient calculator)
    if False:
        stochastic_policy_gradients_no_baseline[hyperparameter_candidates==0] = np.nan
        stochastic_policy_gradients_no_baseline[hyperparameter_candidates==1] = np.nan
        #
        stochastic_policy_gradients_empirical_baseline[hyperparameter_candidates==0] = np.nan
        stochastic_policy_gradients_empirical_baseline[hyperparameter_candidates==1] = np.nan
        #
        stochastic_policy_gradients_optimal_baseline[hyperparameter_candidates==0] = np.nan
        stochastic_policy_gradients_optimal_baseline[hyperparameter_candidates==1] = np.nan
    quiv_0 = axes[0][0].quiver(hyperparameter_candidates[..., 0],
                               hyperparameter_candidates[..., 1],
                               exact_policy_gradients[..., 0],
                               exact_policy_gradients[..., 1],
                               edgecolor='k',
                               facecolor='steelblue',
                               linewidth=quiver_linewidth)
    axes[0][0].set_title(r"Exact $\nabla_{\mathbf{\theta}}\Upsilon$")
    axes[0][0].set_ylabel(r'$\theta_2$')
    axes[0][0].set_box_aspect(1)
    quiv_1 = axes[0][1].quiver(hyperparameter_candidates[..., 0],
                               hyperparameter_candidates[..., 1],
                               stochastic_policy_gradients_no_baseline[..., 0],
                               stochastic_policy_gradients_no_baseline[..., 1],
                               edgecolor='k',
                               facecolor='steelblue',
                               linewidth=quiver_linewidth)
    axes[0][1].set_title(r"No baseline $\widehat{\mathbf{g}}$")
    axes[0][1].set_box_aspect(1)
    quiv_2 = axes[1][0].quiver(hyperparameter_candidates[..., 0],
                               hyperparameter_candidates[..., 1],
                               stochastic_policy_gradients_empirical_baseline[..., 0],
                               stochastic_policy_gradients_empirical_baseline[..., 1],
                               edgecolor='k',
                               facecolor='steelblue',
                               linewidth=quiver_linewidth)
    axes[1][0].set_title(r"Empirical baseline $\widehat{\mathbf{g}}^{\rm b}$")
    axes[1][0].set_ylabel(r'$\theta_2$')
    axes[1][0].set_xlabel(r'$\theta_1$')
    axes[1][0].set_box_aspect(1)
    quiv_3 = axes[1][1].quiver(hyperparameter_candidates[..., 0],
                               hyperparameter_candidates[..., 1],
                               stochastic_policy_gradients_optimal_baseline[..., 0],
                               stochastic_policy_gradients_optimal_baseline[..., 1],
                               edgecolor='k',
                               facecolor='steelblue',
                               linewidth=quiver_linewidth)
    axes[1][1].set_title(r"Optimal baseline $\widehat{\mathbf{g}}^{\rm b_{opt}}$")
    axes[1][1].set_xlabel(r'$\theta_1$')
    axes[1][1].set_box_aspect(1)

    plot_file = os.path.join(output_dir, "TwoD_ToyModel_Gradients.%s" % plots_format.strip('. '))
    fig.savefig(plot_file, dpi=600, bbox_inches='tight')
    print("Saved plot: %s" % plot_file)
    plt.close(fig)


    # 3- Plot the optimization iterations vs. the objective on 1d plot (itereation vs. objective value)
    linewidth = 2.5
    markersize = linewidth * 3
    fig = plt.figure(figsize=(9, 5))
    ax = fig.add_subplot(111)
    utility.show_axis_grids(ax, show_major=True, show_minor=True, major_color='darkgray', minor_color='lightgray')
    ax.plot(no_baseline_optimization_trajectory_objectives,
            '-o',
            color=line_colors[0],
            linewidth=linewidth,
            markersize=markersize,
            label=r"No baseline $\widehat{\mathbf{g}}$"
           )
    ax.plot(empirical_baseline_optimization_trajectory_objectives,
            '--d',
            color=line_colors[1],
            linewidth=linewidth,
            markersize=markersize,
            label=r"Empirical baseline $\widehat{\mathbf{g}}^{\rm b}$"
           )
    ax.plot(optimal_baseline_optimization_trajectory_objectives,
            '-.*',
            color=line_colors[2],
            linewidth=linewidth,
            markersize=markersize,
            label=r"Optimal baseline $\widehat{\mathbf{g}}^{\rm b_{opt}}$"
           )

    xlim = [0, max(no_baseline_optimization_trajectory_objectives.size,
                   empirical_baseline_optimization_trajectory_objectives.size,
                   optimal_baseline_optimization_trajectory_objectives.size
                   )-1
            ]
    xlim[0] -=0.25
    xlim[1] += 0.25
    ax.plot(xlim,
            [bruteforce_winner_design_objective, bruteforce_winner_design_objective],
            '-k',
            linewidth=linewidth*1.25,
            zorder=-100,
            label=r"Global Minimum")
    # Update Ticks and labels
    xticks = ax.get_xticks().astype(int)
    ax.set_xticks(xticks)
    ax.set_xlim(xlim)
    ax.set_xticklabels(xticks)

    ax.set_xlabel(r"Iteration")
    ax.set_ylabel(r"Objective value $\Upsilon(\mathbf{\theta})$")
    ax.legend(loc='best', framealpha=0.75)
    # Save Plot
    plot_file = os.path.join(output_dir, "TwoD_ToyModel_OptimizationIterations.%s" % plots_format.strip('. '))
    fig.tight_layout()
    fig.savefig(plot_file, dpi=600, bbox_inches='tight')
    print("Saved plot: %s" % plot_file)
    plt.close(fig)


    return results



if __name__ == '__main__':

    # Set dimensions
    model_dimension       = 4
    observation_dimension = 2

    # Regularization (update if you want to impose regularization)
    penalty_norm = 0
    penalty_parameter = 0.0

    # Create Inverse problem elements
    prior_stdev       = np.array([2, 1, 0.5, 1])  # good values [4, 2, 1, 3]
    observation_stdev = np.array([0.5, 1])  # good values [1, 3]

    F, B, R = toy_linear.create_inverse_problem_elements(model_dimension=model_dimension,
                                                         observation_dimension=observation_dimension,
                                                         forward_operator_type='average',
                                                         prior_stdev=prior_stdev,
                                                         prior_covariances=False,
                                                         observation_stdev=observation_stdev,
                                                         observation_error_covariances=False)
    # Print Inverse problem elements
    print("Forward Model:")
    print(F)
    print("Prior Covariances:")
    print(B)
    print("Observation Error Covariances:")
    print(R)

    # Test and Plot 1d Toy Problem (See the function arguments if you want to play with it)
    test_2d_design_problem(F=F, B=B, R=R)




