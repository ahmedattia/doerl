

"""
This module shows how to carry out binary OED with 1d toy model.
"""

import sys, os
doerl_path = os.path.abspath('../../')
if not doerl_path in sys.path: sys.path.append(doerl_path)

import matplotlib.pyplot as plt
import seaborn as sns

import doerl
from doerl.utility import utility
from doerl.optimization import optimization
from doerl.stats import bernoulli
from doerl.models.linear import toy_linear
import numpy as np
import pickle


# Generate Linear Model (Trivial matrices) setup, and build prior-> posterior covariances
# Use these to test OED criterion values, behaviour, and optimization

def test_1d_design_problem(F,
                           B,
                           R,
                           batch_size=32,
                           epochs=10,
                           baseline=None,
                           penalty_parameter=0.0,
                           penalty_norm=0,
                           learning_rate=1.0,
                           exclude_bounds=False,
                           antithetic=True,
                           output_dir='PLOTS/OneD_Toy/',
                           plots_format='pdf',
                           verbose=True):
    """
    Assume 2d model, and one candidate sensor location, test the
    OED problem using relaxed and Stochastic-Binary formulation,
    and create suitable plots

    Args:
        F: Forwad operator 1 x n array-like (numpy.ndarray or scipy.sparse.csr_matrix)
        B: Prior Covariances: 2 x 2 matrix
        R: Observation covariances: scalar or 1/1 array
        batch_size:
        baseline: None -->0, 'empirical', 'optimal'
        penalty_parameter:
        learning_rate: default optimization step-size
        exclude_bounds: Ignore 0, 1 from values of the hyperparameter theta
        antithetic:
        output_dir: path to save results/plots to; if None; CWD is used
        plots_format: matplotlib figure format (pdf, png, ...)
        verbose:

    """
    assert np.ndim(F) == 2, "F must be 2d array like with shape (1, *), i.e., one row"
    observation_size, model_size = F.shape
    if observation_size != 1:
        print("This is tailored for 1d observation space only!")
        raise TypeError
    assert B.shape == (model_size, model_size), "B must be  2d array-like with shape (%d, %d)" % (model_size, model_size)
    assert np.asarray(R).size == observation_size, "R must be scalar or 1d array-like with size %d" % observation_size

    #
    # Check baseline, and define objective (lambda just for better readibility)
    obj = lambda w: utility.a_optimality_objective(F=F,
                                           B=B,
                                           R=R,
                                           design=w,
                                           penalty_parameter=penalty_parameter,
                                           penalty_norm=penalty_norm)

    # ****
    # 1- Brute-Force solution; lookup all possible binary designs
    # ****

    # Evaluate objective for all possible bruteforce solutions; here just design is either 0 or 1
    bruteforce_candidates = utility.enumerate_binary(observation_dimension)
    num_bruteforce_candidates = len(bruteforce_candidates)
    bruteforce_objective_values = np.empty(num_bruteforce_candidates)
    if verbose:
        print("Evaluating Objective function for all possible candidate binary designs")
    for i, w in enumerate(bruteforce_candidates):
        bruteforce_objective_values[i] = obj(w)
        if verbose:
            print("Possible Design: [%d/%d]; Objective= %f " %(i+1,
                                                               num_bruteforce_candidates,
                                                               bruteforce_objective_values[i]))

    # Find the design with smallest value of the objective
    # This is complicated only to be used with 2d problems
    winner_ind = tr_argsorter = np.argsort(bruteforce_objective_values)[0]
    bruteforce_winner_design = bruteforce_candidates[winner_ind]
    bruteforce_winner_design_objective = bruteforce_objective_values[winner_ind]
    if verbose:
        print("\n**\nWinner Design: %s\nMinimum Trace: %f\n**" %(bruteforce_winner_design,
                                                                 bruteforce_winner_design_objective))

    # ****
    # 2- Relaxed-OED; Evaluate the objective function for multiple values of the relaxed designs
    # ****

    # Relaxed Design (Standard OED)
    relaxed_design_candidates = np.linspace(0, 1, 50)
    relaxed_design_objective_values = np.array([obj(w) for w in relaxed_design_candidates])


    # ****
    # 3- Stochastic-Binary OED; The proposed approach
    #    Apply without baseline; with empirical baseline, and with optimal baseline
    # ****
    # i  ) Evaluate the objective value for several possible values of the hyperparam theta in [0,1]
    # ii ) Evaluate the exact gradient at all possible values of the hyperparam theta
    # iii) Evaluate the stochastic gradient at all possible values of the hyperparameter
    #      (theta) for each case of baseline above, i.e., None, 'empirical', 'optimal'
    #
    # Possible values of the hyperparameter theta (probability of activating design(s) ),
    # and the corresponding value of the stochastic objective function E(J(design|theta))
    # ****

    # 3.i  )
    # TODO: For 2D, replace with a 2d mesh
    hyperparameter_candidates = np.linspace(0, 1, 50, endpoint=True)
    if exclude_bounds: hyperparameter_candidates = hyperparameter_candidates[1: -1]
    num_hyperparam_candidates = hyperparameter_candidates.size
    hyperparameter_candidates_objective_values = np.empty(num_hyperparam_candidates)
    if verbose:
        print("Evaluating Stochastic-Objective function for multiple values of the hyperparameter")
    for i, theta in enumerate(hyperparameter_candidates):
        hyperparameter_candidates_objective_values[i] = bernoulli.expect(theta, obj)
        if verbose:
            print("Candidate Hyperparameter [%d/%d]; Stochastic-Objective: %f " % (i+1,
                 num_hyperparam_candidates, hyperparameter_candidates_objective_values[i]))

    # print(hyperparameter_candidates, hyperparameter_candidates_objective_values)
    winner_ind = np.argsort(hyperparameter_candidates_objective_values)[0]
    winner_hyperparameter = hyperparameter_candidates[winner_ind]
    winner_hyperparameter_objective = hyperparameter_candidates_objective_values[winner_ind]
    if verbose:
        print("\n**\nWinner Hyperparameter: %s\nMinimum Objective: %f\n**" %(winner_hyperparameter,
                                                                 winner_hyperparameter_objective))

    # 3.ii )
    # Gradient at multiple values of the hyperparameter; the full-expectation
    exact_policy_gradients = np.empty((num_hyperparam_candidates, observation_size))

    if verbose:
        print("Evaluating exact policy gradient multiple values of the hyperparameter")
    for i, theta in enumerate(hyperparameter_candidates):
        exact_policy_gradients[i, :] = optimization.exact_policy_gradient(J=obj,
                                                                          theta=theta,
                                                                          verbose=verbose)

    # 3.ii ) Stochastic approximation of the gradient
    stochastic_policy_gradients_no_baseline = np.empty((num_hyperparam_candidates, observation_size))
    stochastic_policy_gradients_empirical_baseline = np.empty((num_hyperparam_candidates, observation_size))
    stochastic_policy_gradients_optimal_baseline = np.empty((num_hyperparam_candidates, observation_size))

    if verbose:
        print("Evaluating sochastic policy gradient multiple values of the hyperparameter")
        print("** No Baseline **")
    for i, theta in enumerate(hyperparameter_candidates):
        stochastic_policy_gradients_no_baseline[i, :] = optimization.stochastic_policy_gradient(J=obj,
                                                                                    theta=theta,
                                                                                    batch_size=batch_size,
                                                                                    antithetic=False,
                                                                                    verbose=verbose)
    # calcaulate empirical baseline (constant)
    empirical_baseline = (obj(0) + obj(1)) / 2.0
    baseline_objective = lambda w: obj(w) - empirical_baseline
    if verbose:
        print("** empirical/Empirical Baseline **")
        print("\t baseline = %f" % empirical_baseline)
    for i, theta in enumerate(hyperparameter_candidates):
        stochastic_policy_gradients_empirical_baseline[i, :] = optimization.stochastic_policy_gradient(J=baseline_objective,
                                                                                                       theta=theta,
                                                                                                       batch_size=batch_size,
                                                                                                       antithetic=antithetic,
                                                                                                       verbose=verbose)

    if verbose:
        print("** Optimal Baseline **")
    for i, theta in enumerate(hyperparameter_candidates):
        optimal_baseline = optimization.optimal_baseline_estimate(J=obj,
                                                                  theta=theta,
                                                                  batch_size=batch_size,
                                                                  epochs=epochs,
                                                                  antithetic=antithetic,
                                                                  verbose=verbose
                                                                  )
        baseline_objective = lambda w: obj(w) - optimal_baseline
        stochastic_policy_gradients_optimal_baseline[i, :] = optimization.stochastic_policy_gradient(J=baseline_objective,
                                                                                                     theta=theta,
                                                                                                     batch_size=batch_size,
                                                                                                     antithetic=antithetic,
                                                                                                     verbose=verbose)


    # Test the stochastic-binary OED (REINFORCE) Algorithm
    init_theta = np.ones(observation_size) * 0.5
    # No-Baseline
    _, _, _, traject, _, _, _, _ = optimization.binary_REINFORCE(J=obj,
                                                        init_theta=init_theta,
                                                        def_stepsize=learning_rate,
                                                        batch_size=batch_size,
                                                        max_iter=1000,
                                                        pgtol=1e-8,
                                                        baseline=None,
                                                        monitor_iterations=True,
                                                        antithetic=antithetic,
                                                        verbose=verbose)
    no_baseline_optimization_trajectory = np.array(traject)
    no_baseline_optimization_trajectory_objectives = np.array([bernoulli.expect(x, obj) for x in traject])

    # Empirical/Huristic-Baseline
    _, _, _, traject, _, _, _, _ = optimization.binary_REINFORCE(J=obj,
                                                        init_theta=init_theta,
                                                        def_stepsize=learning_rate,
                                                        batch_size=batch_size,
                                                        max_iter=1000,
                                                        pgtol=1e-8,
                                                        baseline='empirical',
                                                        monitor_iterations=True,
                                                        antithetic=antithetic,
                                                        verbose=verbose)
    empirical_baseline_optimization_trajectory = np.array(traject)
    empirical_baseline_optimization_trajectory_objectives = np.array([bernoulli.expect(x, obj) for x in traject])

    # OptimalBaseline
    _, _, _, traject, _, _, _, _ = optimization.binary_REINFORCE(J=obj,
                                                        init_theta=init_theta,
                                                        def_stepsize=learning_rate,
                                                        batch_size=batch_size,
                                                        max_iter=1000,
                                                        pgtol=1e-8,
                                                        baseline='optimal',
                                                        monitor_iterations=True,
                                                        antithetic=antithetic,
                                                        verbose=verbose)
    optimal_baseline_optimization_trajectory = np.array(traject)
    optimal_baseline_optimization_trajectory_objectives = np.array([bernoulli.expect(x, obj) for x in traject])

    # Create a dictionary, save results, and return after plotting
    #
    # Prepare output directory
    if output_dir is None:
        output_dir = os.getcwd()
    if not os.path.isdir(output_dir): os.makedirs(output_dir)
    #
    results = dict(
        # Book Keep Results
        F=F,
        B=B,
        R=R,
        # Bruteforce for Designs
        bruteforce_candidates=bruteforce_candidates,
        bruteforce_objective_values=bruteforce_objective_values,
        bruteforce_winner_design=bruteforce_winner_design,
        bruteforce_winner_design_objective=bruteforce_winner_design_objective,
        #
        # Relaxed Design
        relaxed_design_candidates=relaxed_design_candidates,
        relaxed_design_objective_values=relaxed_design_objective_values,
        #
        # Bruteforce for Hyperparameters
        hyperparameter_candidates=hyperparameter_candidates,
        hyperparameter_candidates_objective_values=hyperparameter_candidates_objective_values,
        winner_hyperparameter=winner_hyperparameter,
        winner_hyperparameter_objective=winner_hyperparameter_objective,
        #
        # Gradient evaluation/estimation results
        exact_policy_gradients=exact_policy_gradients,
        stochastic_policy_gradients_no_baseline=stochastic_policy_gradients_no_baseline,
        stochastic_policy_gradients_empirical_baseline=stochastic_policy_gradients_empirical_baseline,
        stochastic_policy_gradients_optimal_baseline=stochastic_policy_gradients_optimal_baseline,
        #
        # Stochastic-Optimization Results
        no_baseline_optimization_trajectory=no_baseline_optimization_trajectory,
        no_baseline_optimization_trajectory_objectives=no_baseline_optimization_trajectory_objectives,
        empirical_baseline_optimization_trajectory=empirical_baseline_optimization_trajectory,
        empirical_baseline_optimization_trajectory_objectives=empirical_baseline_optimization_trajectory_objectives,
        optimal_baseline_optimization_trajectory=optimal_baseline_optimization_trajectory,
        optimal_baseline_optimization_trajectory_objectives=optimal_baseline_optimization_trajectory_objectives
    )

    # Dump Results;
    results_file = os.path.join(output_dir, "OneD_ToyModel_Results.pickle")
    pickle.dump(results, open(results_file, 'wb'))
    print("Results pickled to: %s" % results_file)

    # Start Plotting; I'm using seaborn, it's fun!
    sns_palette    = 'tab10'
    sns_context    = 'paper'
    sns_style      = 'whitegrid'
    sns_font       = 'sans-serif'
    sns_font_scale = 2
    sns_linewidth  = 3
    # Set Seaborn configs
    sns.set_theme(context=sns_context,
                  style=sns_style,
                  font=sns_font,
                  font_scale=sns_font_scale,
                  rc={'text.usetex':True},
                  color_codes=True
                  )
    #
    # 1- Plots Objective functions (Exact with relaxation and stochastic)
    # *- Add optimal design values and hyperparameters to the plot above
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    sns.lineplot(x=relaxed_design_candidates,
                 y=relaxed_design_objective_values,
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"Relaxed design $\xi$"
                 )
    ax.lines[-1].set_linestyle('--')

    ax = sns.lineplot(x=hyperparameter_candidates,
                      y=hyperparameter_candidates_objective_values,
                      ax=ax,
                      palette=sns_palette,
                      linewidth=sns_linewidth,
                      label=r"Hyperparameter $\theta$"
                      )
    ax.lines[-1].set_linestyle('-')

    # Mark winners
    sns.scatterplot(x=bruteforce_winner_design,
                    y=[bruteforce_winner_design_objective],
                    palette=sns_palette,
                    s=16**2,
                    label=r"$\xi^{\rm opt}$",
                    )
    sns.scatterplot(x=[winner_hyperparameter],
                    y=[winner_hyperparameter_objective],
                    palette=sns_palette,
                    s=13**2,
                    label=r"$\theta^{\rm opt}$",
                    )

    # ax.set_xlabel("Design / Hyperparameter")
    ax.set_ylabel(r"Objective value")
    # Save Plot
    plot_file = os.path.join(output_dir, "OneD_ToyModel_Objective_Values.%s" % plots_format.strip('. '))
    fig.savefig(plot_file, dpi=600)
    print("Saved plot: %s" % plot_file)
    plt.close(fig)


    # 2- Plot All gradients given the values of the hyperparameter (0, 1) on one plot
    fig = plt.figure(figsize=(8, 7))
    ax = fig.add_subplot(111)
    sns.lineplot(x=hyperparameter_candidates,
                 y=exact_policy_gradients.flatten(),
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"Exact $\nabla_{\theta}\Upsilon$"
                 )
    ax.lines[-1].set_linestyle('-')
    sns.lineplot(x=hyperparameter_candidates,
                 y=stochastic_policy_gradients_no_baseline.flatten(),
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"No baseline $\widehat{\mathbf{g}}$"
                 )
    ax.lines[-1].set_linestyle('--')
    sns.lineplot(x=hyperparameter_candidates,
                 y=stochastic_policy_gradients_empirical_baseline.flatten(),
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"empirical baseline $\widehat{\mathbf{g}}^{\rm b}$"
                 )
    ax.lines[-1].set_linestyle(':')
    sns.lineplot(x=hyperparameter_candidates,
                 y=stochastic_policy_gradients_optimal_baseline.flatten(),
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"Optimal baseline $\widehat{\mathbf{g}}^{\rm b_{opt}}$"
                 )
    ax.lines[-1].set_linestyle('-.')

    ax.set_xlabel(r"$\theta$")
    ax.set_ylabel(r"Gradient value/estimate")
    # Save Plot
    plot_file = os.path.join(output_dir, "OneD_ToyModel_Objective_Gradients.%s" % plots_format.strip('. '))
    fig.savefig(plot_file, dpi=600)
    print("Saved plot: %s" % plot_file)
    plt.close(fig)

    # 3- Plot the optimization iterations vs. the objective on 1d plot (itereation vs. objective value)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111)
    sns.lineplot(x=np.arange(no_baseline_optimization_trajectory_objectives.size, dtype=int),
                 y=no_baseline_optimization_trajectory_objectives,
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"No baseline $\widehat{\mathbf{g}}$"
                 )
    ax.lines[-1].set_linestyle('--')
    ax.lines[-1].set_marker('o')
    ax.lines[-1].set_markersize(3*sns_linewidth+5)
    sns.lineplot(x=np.arange(empirical_baseline_optimization_trajectory.size, dtype=int),
                 y=empirical_baseline_optimization_trajectory_objectives,
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"Empirical baseline $\widehat{\mathbf{g}}^{\rm b}$"
                 )
    ax.lines[-1].set_linestyle(':')
    ax.lines[-1].set_marker('d')
    ax.lines[-1].set_markersize(3*sns_linewidth+2)
    sns.lineplot(x=np.arange(empirical_baseline_optimization_trajectory_objectives.size, dtype=int),
                 y=optimal_baseline_optimization_trajectory_objectives,
                 palette=sns_palette,
                 ax=ax,
                 linewidth=sns_linewidth,
                 label=r"Optimal baseline $\widehat{\mathbf{g}}^{\rm b_{opt}}$"
                 )
    ax.lines[-1].set_linestyle('-.')
    ax.lines[-1].set_marker('*')
    ax.lines[-1].set_markersize(3*sns_linewidth)

    # Update Ticks and labels
    xticks = ax.get_xticks().astype(int)
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticks)
    ax.set_xlabel(r"Iteration")
    ax.set_ylabel(r"Objective value")
    # Save Plot
    plot_file = os.path.join(output_dir, "OneD_ToyModel_OptimizationIterations.%s" % plots_format.strip('. '))
    fig.tight_layout()
    fig.savefig(plot_file, dpi=600)
    print("Saved plot: %s" % plot_file)
    plt.close(fig)


    return results



if __name__ == '__main__':

    # Set dimensions
    model_dimension       = 2
    observation_dimension = 1

    # Regularization (update if you want to impose regularization)
    penalty_norm = 0
    penalty_parameter = 0.0

    # Create Inverse problem elements
    prior_stdev       = np.array([2, 1])
    observation_stdev = 1

    F, B, R = toy_linear.create_inverse_problem_elements(model_dimension=model_dimension,
                                                         observation_dimension=observation_dimension,
                                                         forward_operator_type='average',
                                                         prior_stdev=prior_stdev,
                                                         prior_covariances=False,
                                                         observation_stdev=observation_stdev,
                                                         observation_error_covariances=False)
    # Print Inverse problem elements
    print("Forward Model:")
    print(F)
    print("Prior Covariances:")
    print(B)
    print("Observation Error Covariances:")
    print(R)

    # Test and Plot 1d Toy Problem (See the function arguments if you want to play with it)
    test_1d_design_problem(F=F, B=B, R=R)





