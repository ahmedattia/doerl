
from ...utility import utility

import numpy as np
import scipy as sp
import re

import skimage
import skimage.transform


def create_radon_forward_operator(n_voxels=100, n_angles=30, theta=None, scale_matrix=True, verbose=True):
    """
    Construct a radon-transoform matrix A for multiple projection angles.
    Each row of A, takes as an input an image (flattened out as a vector), and create a
    Args:
        n_voxels: Number of pixels along each edge of the image
        n_angles: Number of angles at which to probe the object
            used to create theta (if None found); as linearly spaced <n_angles> angles between 1, 360
            this is overridden if theta is not None
        theta: If passed, array-like objects that contains angles (all must be between 0, 360)
            overridess n_angles  if passed
        scale_matrix: default True; scale A, by maximum sum of rows!

    Returns:
        2D Numpy array representing the radon transform (Linear model for an image):

    """
    if theta is not None:
        theta = np.asarray(theta).flatten()
        theta[theta==360] = 0
        theta = np.unique(theta)
        num_angles = theta.size
        assert n_angles > 0, "The passsed array 'theta' can't be empty!"
        assert (theta[0]>=0 and theta[-1]<360),  "theta values must be in the interval [0, 360)!"

    else:
        assert n_angles > 0, "Number of angles can't be negative!"
        theta = np.linspace(0, 360, n_angles, endpoint=False)

    # Construct radon transform array:
    if verbose:
        print("Creating Forward Model (Radon Transofrms...)")
    A = np.empty((n_voxels*theta.size, n_voxels**2))
    image = np.zeros((n_voxels, n_voxels))
    count = 0
    for i in range(n_voxels):
        for j in range(n_voxels):
            # compute radon transform, and update matrix A
            image[i, j] = 1
            A[:, i*n_voxels+j] = skimage.transform.radon(image, theta=theta, circle=True).flatten()
            image[i, j] = 0  # reset image

    if verbose:
        print("Scaling..")
    if scale_matrix:
        A /= np.max(np.sum(A, 1))

    if verbose:
        print("Done.")

    return A


def get_active_indexes(image_n_voxels, obs_n_angles, design):
    """
    Given number of voxels,  the observation angles,  and a binary design; find the indexes (in sinogram or rows of F to activate)

    Args:
        image_n_voxels
        obs_n_angles
        design: binry design of the same length as obs_angles

    Returns:
        active_indexes: indexes in the output of F that are to be  observed according to the design
        inactive_indexes: indexes in the output of F that are not to be observed according to the design

    """
    design = np.asarray(design, dtype=bool).flatten()
    assert obs_n_angles ==  design.size, "design must be of size equal to obs_n_angles!"

    observation_dimension = obs_n_angles * image_n_voxels
    active_indexes = []
    for ind in np.where(design)[0]:
        active_indexes.append(np.arange(ind*image_n_voxels, (ind+1)*image_n_voxels ))
    active_indexes = np.array(active_indexes).flatten()
    inactive_indexes = np.lib.setdiff1d(np.arange(observation_dimension), active_indexes)
    return active_indexes, inactive_indexes


def _apply_forward_operator_image(F, image, design=None):
    """
    """
    if image.size != np.size(F, 1):
        print("Image  size  doesn't match the forward  operator F")
        print("expected an image of *flattened* size %d " % image.size)
        raise TypeError

    if design is None:
        design = np.asarray(design, dtype=bool)
        obs_n_angles = design.size
        image_n_voxels = np.size(F, 0) // obs_n_angles

        active_indexes, _ = get_active_indexes(image_n_voxels=image_n_voxels,
                                               obs_n_angels=obs_n_angles,
                                               design=design)
        obs = F[active_indexes, :].dot(image.flatten())
    else:
        obs = F.dot(image.flatten())

    return obs

def _apply_forward_operator_array(F, mat, design=None):
    """
    """
    if  np.size(mat, 0) != np.size(F, 1):
        print("input array number of array  doesn't match the forward  operator F")
        print("expected an 2d array with number  of rws %d " % np.size(mat, 0) )
        raise TypeError

    if design is None:
        design = np.asarray(design, dtype=bool)
        obs_n_angles = design.size
        image_n_voxels = np.size(F, 0) // obs_n_angles

        active_indexes, _ = get_active_indexes(image_n_voxels=image_n_voxels,
                                               obs_n_angels=obs_n_angles,
                                               design=design)
        proj_mat = F[active_indexes, :].dot(mat)
    else:
        proj_mat = F.dot(mat)

    return proj_mat

def _FAFt(F, A, design=None):
    """
    """
    if  np.size(mat, 0) != np.size(F, 1):
        print("input array number of array  doesn't match the forward  operator F")
        print("expected an 2d array with number  of rws %d " % np.size(mat, 0) )
        raise TypeError

    if design is None:
        design = np.asarray(design, dtype=bool)
        obs_n_angles = design.size
        image_n_voxels = np.size(F, 0) // obs_n_angles

        active_indexes, _ = get_active_indexes(image_n_voxels=image_n_voxels,
                                               obs_n_angels=obs_n_angles,
                                               design=design)
        proj_mat = F[active_indexes, :].dot(mat)
    else:
        proj_mat = F.dot(mat)

    return proj_mat

def apply_forward_operator(F, x, design=None, apply_twice=True):
    """
    Apply forward operator (radon transform) to x

    Args:
        F: forward operator
        x: either 1d *flattened* image, or 2d array
        apply_twice: only used if x is a 2d array; in this  case, FxF^T is calculated; followed by projection based on the design from both sides

    """
    x = np.asarray(x)
    if np.ndim(x) == 1:
        out = _apply_forward_operator_image(F=F, image=x, design=desig)
    elif  np.ndim(x) == 2:
        out = _apply_forward_operator_array(F=F, image=x, design=design)
        if apply_twice:
            out = _apply_forward_operator_array(F=F, image=out.T, design=design).T
    else:
        print("x must be 1d or 2d array")
        raise TypeError

    return out


def create_inverse_problem_elements(image_n_voxels=100,
                                    obs_n_angles=30,
                                    obs_theta=None,
                                    scale_radon_matrix=True,
                                    obs_noise_level=0.10,
                                    prior_stdev=0.1,
                                    prior_covariances=False,
                                    observation_stdev=0.015,
                                    observation_error_covariances=False,
                                    allow_sparse=True,
                                    verbose=False
                                    ):
    """
    Create the basic elements of Bayesian inverse problem:
        1- Fowrad operator (wrapper around create_radon_forwrad_operator)
        2- Prior mean, Prior Covariance (xb, B)
        3- Observation Error Covariane (R)

    Args:
        image_n_voxels: Number of pixels along each edge of the image
        obs_n_angles: Number of angles at which to probe the object
            used to create theta (if None found); as linearly spaced <n_angles> angles between 1, 360
            this is overridden if theta is not None
        obs_theta: If passed, array-like objects that contains angles (all must be between 0, 360)
            overridess n_angles  if passed
        scale_radon_matrix:
        obs_noise_level:
        prior_stdev : diagonal of the prior covariance matrix (if None, choosen randomly)
            i   - None
            ii  - scalar; all diagonal entries are equal, and set to this value
            iiii- iterable (e.g., list or 1d array) full diagonal
        prior_covariances : create full prior covariance matrix (diagonal if False), based on the created diagonal
        observation_stdev : same as 'prior_stdev', but use to create observation error covariance matrix
        observation_error_covariances : same as 'prior_covariances' but applies to observation error covariances
        allow_sparse : return sparce data structures (e.g., scipy.sparce.csr_matrix vs numpy.ndarray (s))

    Returns:
        F: the forwrad operator matrix; 2d array-like of size (observation_dimension x model_dimension)
        B: Prior Covariance matrix; 2d array-like of size (model_dimension x model_dimension)
        R: Observation error covariance matrix; 2d array-like of size (observation_dimension x observation dimension)

    """

    # Forward Operator/Model (Parameter to Observable Map
    F = create_radon_forward_operator(n_voxels=image_n_voxels,
                                      n_angles=obs_n_angles,
                                      theta=obs_theta,
                                      scale_matrix=scale_radon_matrix,
                                      verbose=verbose)

    # Covariance Matrices
    # State
    model_dimension = np.size(F, 1)
    B = utility.create_covariance_matrix(size=model_dimension,
                                         corr=prior_covariances,
                                         stdev=prior_stdev)

    # Observation
    observation_dimension = np.size(F, 0)
    R = utility.create_covariance_matrix(size=observation_dimension,
                                         corr=observation_error_covariances,
                                         stdev=observation_stdev)

    if allow_sparse:
        F = sp.sparse.csr_matrix(F)
        B = sp.sparse_csr_matrix(B)
        R = sp.sparse_csr_matrix(R)

    return F, B, R

def get_shepp_logan_phantom_example(n_voxels=100, model='reflect', multichannel=False, flatten=True):
    """
    """
    # Load and rescale Shepp Logan Phantom Image
    image = skimage.data.shepp_logan_phantom()
    image = skimage.transform.rescale(image,
                                      scale=(n_voxels/image.shape[0]),
                                      mode=model,
                                      multichannel=multichannel)
    if flatten:
        image = image.flatten()
    return image

def observation_noise_variance(F, image, noise_level=0.10):
    """
    """
    obs = F.dot(image.flatten())

    # Noisy data: sinogram + random noise
    noise_variance = ( noise_level * np.sqrt(np.mean(np.square(obs))) ) ** 2
    return noise_variance



