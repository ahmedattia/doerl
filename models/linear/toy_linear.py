

"""
This module provides functionality to create and test toy models performance.
"""

from ...utility import utility

import numpy as np
import scipy as sp
import re


def create_inverse_problem_elements(model_dimension=2,
                                    observation_dimension=2,
                                    forward_operator_type='identity',
                                    prior_stdev=None,
                                    prior_covariances=False,
                                    observation_stdev=None,
                                    observation_error_covariances=False,
                                    allow_sparse=False
                                   ):
    """
    Create the basic elements of Bayesian inverse problem:
        1- Prior mean, Prior Covariance (xb, B)
        2- Observation Error Covariane (R)
        3-

    Args:
        model_dimension : size of the model state (number of columns in F)
        observation_dimension : size of the observation space (number of rows in F)
        forward_operator_type : Type of the Forwrad operator F
            i  - 'identity': F = I
            ii - 'first'   : first (observation_dimension) elements from the state
            iii- 'average' : average of consequtive pairs (avg(x1, x2), avg(x3, x4), ...);
                Here, observation_size is discarded
        prior_stdev : diagonal of the prior covariance matrix (if None, choosen randomly)
            i   - None
            ii  - scalar; all diagonal entries are equal, and set to this value
            iiii- iterable (e.g., list or 1d array) full diagonal
        prior_covariances : create full prior covariance matrix (diagonal if False), based on the created diagonal
        observation_stdev : same as 'prior_stdev', but use to create observation error covariance matrix
        observation_error_covariances : same as 'prior_covariances' but applies to observation error covariances
        allow_sparse : return sparce data structures (e.g., scipy.sparce.csr_matrix vs numpy.ndarray (s))

    Returns:
        F: the forwrad operator matrix; 2d array-like of size (observation_dimension x model_dimension)
        B: Prior Covariance matrix; 2d array-like of size (model_dimension x model_dimension)
        R: Observation error covariance matrix; 2d array-like of size (observation_dimension x observation dimension)

    """
    assert observation_dimension <= model_dimension, "Observation dimension can't exceed model state dimension!"

    # Forward Operator/Model (Parameter to Observable Map
    if re.match(r'\Aidentity\Z', forward_operator_type, re.IGNORECASE):
        observation_dimension = model_dimension
        if allow_sparse:
            F = sp.sparse.spdiags(np.ones(model_dimension), 0, model_dimension, model_dimension)
        else:
            F = np.eye(model_dimension)

    elif re.match(r'\Afirst\Z', forward_operator_type, re.IGNORECASE):
        if allow_sparse:
            F = sp.sparse.spdiags(np.ones(observation_dimension), 0, observation_dimension, model_dimension)
        else:
            F = np.zeros((observation_dimension, model_dimension))
            inds = np.arange(observation_dimension, dtype=int)
            F[(inds, inds)] = 1

    elif re.match(r'\Aaverage\Z', forward_operator_type, re.IGNORECASE):
        if model_dimension < 2:
            print("Average operator requires at least 2D model")
            raise ValueError
        observation_dimension = model_dimension // 2
        F = np.zeros((observation_dimension, model_dimension))
        inds_s = np.arange(0, model_dimension//2*2, 2, dtype=int)
        inds_o = np.arange(observation_dimension, dtype=int)
        F[(inds_o, inds_s)] = 0.5
        F[(inds_o, inds_s+1)] = 0.5
        #
        if allow_sparse:
            F = sp.sparse.csr_matrix(F)
    else:
        print("Unknown forward operator type '%s'" % forward_operator_type)
        raise ValueError

    # Covariance Matrices
    B = utility.create_covariance_matrix(size=model_dimension,
                                         corr=prior_covariances,
                                         stdev=prior_stdev)

    R = utility.create_covariance_matrix(size=observation_dimension,
                                         corr=observation_error_covariances,
                                         stdev=observation_stdev)

    return F, B, R


