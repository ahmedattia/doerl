
import os
import re

import itertools
from numbers import Number
from functools import reduce
import operator as op

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as splinalg

import matplotlib


# Statistics:
# -----------
def calculate_rmse(first, second):
    """
    Calculate the root mean squared error between two vectors of the same type.

    Args:
        first:
        second:

    Returns:
        rmse

    """
    if not isinstance(first, type(second)):
        print('in Utility.calculate_rmse: Type of first entry: %s ' % type(first))
        print('in Utility.calculate_rmse: Type of second entry: %s ' % type(second))
        raise TypeError(" The two vectors must be of the same type!"
                        "First: %s\n"
                        "Second: %s" % (repr(type(first)), repr(type(first))))
    first = np.array(first).flatten()
    second = np.array(second).flatten()
    if first.size != second.size:
        print("Vectors must be of equal size!")
        raise TypeError

    diff = first - second
    err = np.sum(diff**2)
    rmse = np.sqrt(err) / np.sqrt(diff.size)
    return rmse

def factorize_covariances(covariance, lower=True):
    """
    Genereate Cholesky decomposition of the passed covariance matrix/kernel
    For scalar covariance, this is the standard deviation
    For matrices, this is the lower triangular matrix by default

    """
    if isnumber(covariance):
        std = np.sqrt(covariance)
    elif isinstance(covariance, np.ndarray):
        std = sp.csc_matrix(np.linalg.cholesky(covariance))
        if not lower:
            std = std.T
        std = std.toarray()
    elif sp.issparse(covariance):
        n = covariance.shape[0]
        LU = splinalg.splu(covariance, diag_pivot_thresh=0)
        # Check the matrix is positive definite:
        if (LU.perm_r==np.arange(n)).all() and (LU.U.diagonal()>0).all():
           std = LU.L.dot(sp.diags(LU.U.diagonal()**0.5))
           if not lower:
               std = std.T
        else:
            print("The matrix is not positive definite")
            raise ValueError
    elif callable(covariance):
        print("TODO...")
        raise NotImplementedError
    else:
        print("covariance must be scalar, numpy array, or scipy  sparse matrix")
        print("Received %s " % type(covariance))
        raise TypeError
    return std

def create_covariance_matrix(size=2, corr=False, stdev=None):
    """
    construct a numpy covariance matrix of dimension (size x size),
    with/without correlations
    """
    if size < 1:
        print("covariance size must be at least 1")
        raise ValueError

    if stdev is not None:
        stdev = np.array(stdev).flatten()
    else:
        stdev = 1e-5 + np.random.rand(size)  # 0<1e-5<= standard deviations <= 1

    if 1 == stdev.size < size:
        stdev = np.ones(size) * stdev
    elif stdev.size == size:
        pass
    else:
        print("size mismatches length of passed standard deviations")
        raise ValueError

    # construct reasonable correlation matrix
    # non-Diagonal covariance matrix
    if corr:
        cov = np.eye(size)
        for i in range(size-1):
            coeff =  0.95 * np.random.rand(size-(i+1))  # 0<=coeff<=95
            cov[i, i+1: ] += coeff
            cov[i+1: , i] += coeff
        cov *= np.outer(stdev, stdev)  # scale the correlation matrix
    else:
        cov = np.diag(stdev ** 2)  # Diagonal covariance matrix

    return cov

def posterior_covariance_matrix(B, R=None, F=None, w=None):
    """
    Assuming Gaussianity, and given prior covariance matrix,
    and observation covariance matrix, construct the posterior covariance matrix

    Args:
        B: Prior Covariance matrix
        R: Observation error covariance matrix
        F: Forward model: y = F x + eta; eta ~ N(0, R); F is set to I if None is passed
        w: Design variable; set to 1 if None is Passed

    Returns
        A: Posterior covariance matrix $(F^T R^{-1/2} W R^{-1/2} F + B^{-1})^{-1}$

    """
    # Asert and get sizes
    if F is R is None:
        pass

    elif F is None:
        if B.shape != R.shape:
            print("""here, B, R must be of similar shapes;
                  otherwise observation operator will be required""")
            raise ValueError
        Ns, No = B.shape
        if Ns!=No:
            print("Covariance matrix must be square!")
            raise ValueError
    else:
        No, Ns = F.shape
        assert B.shape == (Ns, Ns), "Prior covariance shape is incorrect"
        assert R.shape == (No, No), "Observation error covariance shape is incorrect"

    if F is R is None:
        A = B
    else:
        # 1: prior covariance inverse
        Binv = np.linalg.inv(B)

        if w is None:
            w = np.ones(No)
        else:
            w = np.array(w).flatten()
            if w.size == 1:
                w = np.ones(No)*w[0]
        if w.size != No:
            print("design size must be %d, found %d" % (No, w.size))
            raise TypeError

        active_indexes = np.where(w)[0]

        R_inv = np.linalg.inv(R[active_indexes, :][:, active_indexes])
        weighted_Hmisfit = np.dot(F[active_indexes, :].T, R_inv.dot(F[active_indexes, :]))

        # Posterior covariance
        A = np.linalg.inv( Binv + weighted_Hmisfit)

    return A


# Forward and Inverse Problem:
# ------------------------------
def foward_problem(F, x, R=None):
    """
    Apply the forward operator (y = Fx + e) where e~N(0, R)
    """
    if R is not None:
        L = np.linalg.cholesky(R)
    else:
        L = 0
    y = np.dot(F, x)
    y += np.dot(L, np.random.randn(np.size(y)))
    return y

def kalman_filter(xb, y, F, B, R):
    """
    Calculate posterior mean, and covariance matrix,
        given the prior mean, observation(y),
        and prior (B) and observation error (R) covariance matrices

    Returns:
        xa: posterior mode/mean
        A: posterior error covariance matrix

    """
    innov = y - np.dot(F, xb)
    S = np.dot(F, B).dot(F.T) + R
    K = np.dot(B, F.T).dot(np.linalg.inv(S))
    xa = xb + K.dot(innov)
    A = B - K.dot(F).dot(B)

    return xa, A




# Optimal Design of Experiments:
# ------------------------------
def calculate_oed_criterion(post_cov, oed_criterion='A'):
    """
    Given a  matrix post_cov, calculate the optimality criterion

    Args:
        post_cov: the posterior covariance matrix,
            out of which optimality criterion is evaluated
        oed_criterion: 'A' for A-optimality (Trace(post_cov)),
            'D' for D-optimality (log-det (post_cov))

    Returns:
        oed_objective : Trace(post_cov) if oed_criterion is set to 'A',
            log-det(post_cov) if oed_criterion is set to 'D'

    """
    assert np.ndim(post_cov) == 2, "Wrong size; two-dimensional matrix expected"
    assert post_cov.shape[0] == post_cov.shape[1], "Square matrix is expected"

    # Evaluate the OED criterion  value
    if re.match(r'\AA( |_|-)*(opt)*', oed_criterion, re.IGNORECASE):
        oed_objective = np.trace(post_cov)
    elif re.match(r'\AD( |_|-)*(opt)*', oed_criterion, re.IGNORECASE):
        oed_objective = np.log(np.linalg.det(post_cov))
    else:
        print("Unrecognized optimality criterion  %s" % oed_criterion)
        raise ValueError
    #
    return oed_objective

def a_optimality_criterion(F, B, R, design):
    """
    Trace of posterir covariances  with observation noise weighted by the passed design

    Args:
        F: Forwarod operator (State-to-observation matrix)
        B: Background/Prior covariance matrix
        R: Observation Error covariance matrix
        design: binary on/off sensor design vector

    Returns:
        trace of the posterior covariance matrix, where likelihood is modified/projected based on the passed design

    """
    post_covariance = posterior_covariance_matrix(B=B, R=R, F=F, w=design)
    trace = calculate_oed_criterion(post_cov=post_covariance, oed_criterion='A')
    return trace

def a_optimality_objective(F, B, R, design, penalty_parameter=0, penalty_norm=0):
    """
    A-optimality objective = a_optimality_criterion  + penalty * penalty norm

    Args:
        F: Forwarod operator (State-to-observation matrix)
        B: Background/Prior covariance matrix
        R: Observation Error covariance matrix
        design: binary on/off sensor design vector
        penalty_parameter:
        penalty_norm

    Returns:
        trace of the posterior covariance matrix, where likelihood is modified/projected based on the passed design


    """
    obj = a_optimality_criterion(F, B, R, design)
    penalty = np.linalg.norm(np.array(design).flatten(), ord=penalty_norm)
    obj += penalty_parameter * penalty
    return obj


# Plotting & Visualization:
# -------------------------
def plots_enhancer(fontsize=22,
                   fontweight='bold',
                   usetex=False):
    """
    """
    font = {'family':'sans-serif',
            'weight':fontweight,
            'size': fontsize}
    matplotlib.rc('font', **font)
    matplotlib.rc('text', usetex=True)

def show_axis_grids(ax,
                    show_major=True,
                    show_minor=True,
                    major_color='black',
                    minor_color='black'):
    """
    Given an axis, show the major/minor axis grids
    """
    # Don't allow the axis to be on top of your data
    ax.set_axisbelow(True)

    # Turn on the minor TICKS, which are required for the minor GRID
    ax.minorticks_on()

    if show_major:
        # Customize the major grid
        ax.grid(which='major', linestyle='-', linewidth='0.5', color=major_color)

    if show_minor:
        # Customize the minor grid
        ax.grid(which='minor', linestyle=':', linewidth='0.5', color=minor_color)

    return ax

def unique_colors(size, np_seed=2456, gradual=False):
    """
    get uniqe random colors on the format (R, G, B) to be used for plotting with many lines
    """
    # Get the current state of Numpy.Random, and set the seed
    if gradual:
        G_vals = np.linspace(0.0, 0.5, size, endpoint=False)
        R_vals = np.linspace(0.0, 0.3, size, endpoint=False)
        B_vals = np.linspace(0.5, 1, size, endpoint=False)
    else:
        np_rnd_state = np.random.get_state()
        np.random.seed(np_seed)

        G_vals = np.linspace(0.0, 1.0, size, endpoint=False)
        R_vals = np.random.choice(range(size), size=size, replace=False) / float(size)
        B_vals = np.random.choice(range(size), size=size, replace=False) / float(size)

        # Resend Numpy random state
        np.random.set_state(np_rnd_state)

    return [(r, g, b) for r, g, b in zip(R_vals, G_vals, B_vals)]


def aggregate_configurations(configs,
                             def_configs,
                             copy_configurations=True):
    """
    Blindly (and recursively) combine the two dictionaries. One-way copying: from def_configs to configs only.
    Add default configurations to the passed configs dictionary
    This code is borrowed from DATeS package: https://www.geosci-model-dev.net/12/629/2019/

    Args:
        configs:
        def_configs:
        copy_configurations:

    Returns:
        configs:

    """
    if configs is None and def_configs is None:
        raise ValueError("both inputs are None")
    elif configs is None:
        if copy_configurations:
            configs = dict.copy(def_configs)
        else:
            configs = def_configs
    else:
        #
        for key in def_configs:
            if key not in configs:
                configs.update({key: def_configs[key]})
            elif configs[key] is None:
                    configs[key] = def_configs[key]
            elif isinstance(configs[key], dict) and isinstance(def_configs[key], dict):
                # recursively aggregate the dictionary-valued keys
                sub_configs = aggregate_configurations(configs[key], def_configs[key])
                configs.update({key: sub_configs})
    #
    return configs


# Files, Directories & Configs:
# -----------------------------
def get_list_of_files(root_dir,
                      recursive=False,
                      return_abs=True,
                      extension=None):
    """
    Retrieve a list of files in the passed root_dir, (and optionally in all sub-directories).
    This function ignores special files (starting with . or __ ).

    Args:
        root_dir: directory to start constructing sub-directories of.
        recursive: True/False; if True, the passed files are found in subdirectories as well
        return_ab: True/False; if True, returned paths are absolute, otherwise relative (to root_dir)

    Returns:
        files_list: a list containing absolute (or relative) under the given root_dir.

    Raises:
        IOError: If the passed path/directory does not exist
    """
    if not os.path.isdir(root_dir):
        raise IOError(" ['%s'] is not a valid directory!" % root_dir)
    else:
        _passed_abs_path = os.path.abspath(root_dir)
        _passed_rel_path = os.path.relpath(root_dir)

    if extension is None:
        _ext = extension
    else:
        _ext = extension.lower().lstrip('* .').rstrip(' ')
    #
    files_list = []
    if recursive:
        dirs_list = get_list_of_subdirectories(root_dir=_passed_abs_path, return_abs=False, ignore_special=False)
    else:
        dirs_list = [_passed_rel_path]

    for dir_name in dirs_list:
        loc_files = os.listdir(dir_name)
        #
        for fle in loc_files:
            file_rel_path = os.path.relpath(os.path.join(dir_name, fle))  # relative path of file
            if os.path.isfile(file_rel_path):  # To ignore directories...
                if not ('/.' in fle or '__' in fle):
                    # Add this file to the aggregated list:
                    if _ext is not None:
                        head, tail = os.path.splitext(file_rel_path)
                        if not(re.match(r'\A(.)*%s\Z' % _ext, tail, re.IGNORECASE)):
                            continue
                    if return_abs:
                        files_list.append(os.path.abspath(file_rel_path))
                    else:
                        files_list.append(file_rel_path)
            else:  # not a file; pass
                pass  # or continue

    return files_list

def get_list_of_subdirectories(root_dir,
                               ignore_root=False,
                               return_abs=False,
                               ignore_special=True,
                               recursive=True):
    """
    Retrieve a list of sub-directories .

    Args:
        root_dir: directory to start constructing sub-directories of.
        ignore_root: True/False; if True, the passed root_dir is ignored in the returned list
        return_ab: True/False; if True, returned paths are absolute, otherwise relative (to root_dir)
        ignore_special: Ture/False; if True, this function ignores special files (starting with . or __ ).

    Returns:
        subdirs_list: a list containing subdirectories under the given root_dir.

    Returns:
        subdirs_list: list of subdirectories; the subdires are absolute paths or relative paths based the passed root
            returns empty list if root_dir has no subdirectories.

    Raises:
        IOError: If the passed path/directory does not exist
    """
    #
    if not os.path.isdir(root_dir):
        raise IOError(" ['%s'] is not a valid directory!" % root_dir)

    subdirs_list = []
    _passed_path = os.path.abspath(root_dir)
    if os.path.isabs(root_dir):
        pass  # OK, it's an absolute path, good to go
    else:
        # the passed path is relative; convert back, and guarantee it's of the right format for later comparison
        _passed_path = os.path.relpath(_passed_path)

    if recursive:
        for root, _, _ in os.walk(_passed_path):
            # '/.' insures that the iterator ignores any subdirectory of special directory such as '.git' subdirs.
            # '__' insures that the iterator ignores any cashed subdirectory.
            if ignore_special and ('/.' in root or '__' in root):
                continue

            # in case this is not the initial run. We don't want  to add duplicates to the system paths' list.
            if ignore_root and _passed_path == root:
                pass
            else:
                if return_abs:
                    subdirs_list.append(os.path.abspath(root))
                else:
                    subdirs_list.append(os.path.relpath(root))
    else:
        _sub = next(os.walk(_passed_path))[1]
        if not ignore_root:
            if return_abs:
                subdirs_list.append(os.path.abspath(_passed_path))
            else:
                subdirs_list.append(os.path.relpath(_passed_path))
        for d in _sub:
            if return_abs:
                subdirs_list.append(os.path.join(os.path.abspath(_passed_path), d))
            else:
                subdirs_list.append(os.path.join(os.path.relpath(_passed_path), d))

    #
    return subdirs_list

def try_file_name(directory, file_prefix, extension):
    """
    Try to find a suitable file name file_prefix_<number>.<extension>
    """
    #
    if not os.path.isdir(directory):
        raise IOError(" ['%s'] is not a valid directory!" % directory)

    if not directory.endswith('/'):
        directory += '/'

    file_name = file_prefix + '.'+extension.strip('. ')
    if not os.path.isfile(os.path.join(directory, file_name)):
        pass
    else:
        #
        success = False
        counter = 0
        while not success:
            file_name = file_prefix +'_' + str(counter) + '.' + extension.strip('. ')

            if not (os.path.isfile(directory + file_name) ):
                success = True
                break
            else:
                pass
            counter += 1
    return file_name


# Numbers and Enumeration:
# ------------------------
def ncr(n, r):
    """
    Calculate combinations efficiently

    Args:
        n, r: integers

    Returns:
        nCr: n! / (r! (n-r)!)

    """
    r = min(r, n-r)
    if r == 0: return 1
    numer = reduce(op.mul, range(n, n-r, -1))
    denom = reduce(op.mul, range(1, r+1))
    nCr = numer//denom
    return nCr

def isnumber(x, real_only=False):
    """
    Check if a given variable x is a number.
    A number here is defined as one of the following (unless read_only is True):
        - integer
        - float
        - complex number
        - boolean

    Args:
        x: variable to  be checked
        real_only: if True, limit numbers to to the set of integers and floating point numbers

    Returns:
        flag: True/False

    """
    if isinstance(x, Number):
        if real_only and  not isinstance(x, (int, float)):
            flag = False
        else:
            flag =  True
    else:
        flag = False
    return flag

def isiterable(a):
    """
    A simple function to check if
    """
    check = False
    try:
        a.__iter__
        check = True
    except(AttributeError):
        check = False
    return check

def enumerate_binary(size):
    if size > 15:
        print("Wont' do enumeration for all possible combination of more than 25 choices")
        return ValueError
    comb = list(itertools.product([0, 1], repeat=size))
    for i, c in enumerate(comb): comb[i] = np.array(c).flatten()
    return comb

def index_to_binary_state(k, size, dtype=bool):
    """
    Return the binary state=(v_1, v_2, ...) of dimension=size, with index k,
    where the index k is defined by the unique mapping:
        $ k = 1 + \sum_{i=1}^{size} v_i 2^{i-1}$

    Remarks:
        the type of the returned numpy 1d array is set by dtype

    """
    assert k == int(k), "k must be an integer!"
    k = int(k)
    assert k > 0, "k must be positive integer/indes; received %d" % k
    _state = np.fromstring(np.binary_repr(k-1)[::-1], dtype='S1').astype(dtype)
    if _state.size == size:
        state = _state
    elif _state.size < size:
        state = np.zeros(size, dtype=dtype)
        state[: _state.size] = _state
    else:
        print("Passed index k exceeds the dimension set by size")
        raise ValueError
    return state

def index_from_binary_state(state):
    """
    Reverse of "index_to_binary_state"
    Return the index k corresponding to the passed state (of dimension=size),
    where the index k is defined by the unique mapping:
        $ k = 1 + \sum_{i=1}^{size} v_i 2^{i-1}$

    Remarks:
        Entries of the passed state are interpretted as True/False by conversion to bool

    """
    state = np.asarray(state).flatten().astype(int)
    # Assert, and convert to binary
    # k = 1 + state.dot(2**np.arange(state.size))
    k = 1
    for s in range(state.size):
        k += state[s] * 2**s
    return k


def group_by_num_active(results_dict, design_size):
    """
    Given a dictionary (rsults_dict), with keys representing binary index,
    group the values by the size of the design corresponding to each design key
    """
    assert isinstance(results_dict, dict), "results_dict must be a dictionary!"

    sizes = []
    values = []
    for k in results_dict:
        design = index_to_binary_state(k, size=design_size)
        active = int(np.linalg.norm(design, ord=0))
        value  = results_dict[k]

        if active in sizes:
            ind = sizes.index(active)
            values[ind].append(value)
        else:
            sizes.append(active)
            values.append([value])
    return sizes, values























