DOERL: Design of Experiments via Reinforcement Learning
=======================================================

Light package for Binary stochastic optimization problems
emerging in Design of experiments for PDE-constrained Bayesian inverse problems.

The package provides the code that was used to produce numerical results of the paper:
[Stochastic Learning Approach for Binary Optimization: Application to Bayesian Optimal Design of Experiments](https://doi.org/10.1137/21M1404363).

> **_NOTE:_**  
For advanced versions and new developments of our probabilistic optimization, check our [PyOED](https://web.cels.anl.gov/~aattia/pyoed/). 
