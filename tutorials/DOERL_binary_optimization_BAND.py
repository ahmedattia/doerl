#!/usr/bin/env python
# coding: utf-8

# DOERL (Design of experiments using Reinforcement Learning)
#   A quick tutorial to solving binary optimization problem using DOERL


# Import standard Python packages/modules
import sys, os
import numpy as np

# Make DOERL accessible without installation
doerl_path = os.path.join("../../")
if doerl_path not in sys.path: sys.path.append(doerl_path)

# Import DOERL and subpackages/modules used for solving binary optimization problems
import doerl
from doerl.optimization import optimization
from doerl.utility import utility


# Utility/Objective functions to optimize
def utility_func_increasing(design, **kwargs):
    """
    Evaluate the value of the utility function corresponding to the passed design
    This is a trivial function that returns the sum of entries in the design.
        Thus, the optimal solution has all entries ON (upon maximization).
    """
    # Convert to boolean (we only consider binary designs here)
    design = np.asarray(design, dtype=bool)
    result = np.count_nonzero(design)
    return result

def utility_func_split(design, **kwargs):
    """
    Evaluate the value of the utility function corresponding to the passed design
    This is a trivial function that returns the number of active entries if less than or equal to design size divided by 2,
        otherwise it returns number of inactive entries. Thus, objective value is 0, 1, ..., design size // 2.
    This adds nonsmoothness, and make the solution non-degenerate (all designs with ~50% active/ON entries are optimal for max problems)
    """
    # Convert to boolean (we only consider binary designs here)
    design = np.asarray(design, dtype=bool)
    n_active = np.sum(design)
    if n_active <= int(design.size//2):
        result = n_active
    else:
        result = design.size - n_active
    return result


if __name__ == '__main__':
    # Settings:
    DESIGN_SIZE        = 10         # problem dimension; e.g., number of candidate sensors
    MAXIMIZE_OBJECTIVE = True       # Maximize if True, otherwise Minimize (see below)
    BASELINE           = 'optimal'  # Reduce variance of the stochastic estimtor

    # Choose the function to optimize (min by default)
    if MAXIMIZE_OBJECTIVE:
        U = lambda d: -utility_func_split(d)
        # U = lambda d: -utility_func_increasing(d)
    else:
        U = lambda d: utility_func_split(d)
        # U = lambda d: utility_func_increasing(d)

    # Set the initial policy (probability) all sensors have the same probability
    initial_p = np.ones(DESIGN_SIZE) * 0.5

    # Solve the optimization (min) problem using the binary REINFORCE (stochastic learning):
    optimal_policy, optimal_design_sample, optimal_design_sample_objval, \
        traject, converged, msg, objective_value_tracker, \
        sampled_design_indexes = optimization.binary_REINFORCE(U,
                                                               init_theta=initial_p,
                                                               # def_stepsize=0.15,
                                                               # max_iter=15,
                                                               # opt_sample_size=10,
                                                               baseline=BASELINE,
                                                               verbose=False,
                                                              )

    # Print results (nicely)
    print(f"The optimal policy is: {str(optimal_policy)}")
    print(f"Sampled Designs (optimal sample):")
    for xi, u_xi in zip(optimal_design_sample, optimal_design_sample_objval):
        opt_U = -u_xi if MAXIMIZE_OBJECTIVE else u_xi
        print(f"Design (d): {str(xi.astype(int))}; U(d): {opt_U}")


    # Simple functions for enumeration and other useful tasks are provided
    # E.g., solution by bruteforce (for reasonably small problems) to test quality of the solution
    if DESIGN_SIZE <= 15:
        bruteforce_results = optimization.binary_bruteforce_objective(U, num_candidates=DESIGN_SIZE)
        bruteforce_objvals = [bruteforce_results[k] for k in bruteforce_results.keys()]
        opt_objval         = np.min(bruteforce_objvals)
        opt_U              = -opt_objval if MAXIMIZE_OBJECTIVE else opt_objval

        # Print all designs along with evaluted objective found by bruteforce
        print("Bruteforce Results (Binary Optimization)")
        for index in bruteforce_results.keys():
            objval = bruteforce_results[index]
            if objval == opt_objval:
                xi = utility.index_to_binary_state(index, size=DESIGN_SIZE).astype(int)
                print(f"Optimal Design (d): {str(xi)}; U(d): {opt_U}")


