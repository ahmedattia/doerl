{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# DOERL (Design of experiments using Reinforcement Learning)\n",
    "## A quick tutorial to solving binary optimization problem using DOERL\n",
    "\n",
    "The goal is to efficiently solve the following binary optimization problem:\n",
    "\n",
    "$$\n",
    "    \\underset{\\xi\\in\\{0, 1\\}^{N_s}}{\\operatorname{arg\\,max}} \\mathcal{U}(\\xi,\\cdot)\\,,\n",
    "$$\n",
    "by solving the alternative stochastic optimization problem:\n",
    "$$\n",
    "    \\underset{\\mathbf{p}\\in[0, 1]^{N_s}}{\\operatorname{arg\\,max}} \\,\\, \\mathbb{E}_{\\xi\\sim P(\\xi\\,|\\,\\mathbf{p})}[\\mathcal{U}(\\xi,\\cdot)]\\,,\n",
    "$$\n",
    "where $P(\\xi\\,|\\,\\mathbf{p})$ is a multivariate Bernoulli distribution with probability of success/activation $\\mathbf{p}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import standard Python packages/modules\n",
    "import sys, os\n",
    "import numpy as np\n",
    "\n",
    "# Make DOERL accessible without installation\n",
    "doerl_path = os.path.join(\"../../\")\n",
    "if doerl_path not in sys.path: sys.path.append(doerl_path)\n",
    "\n",
    "# Import DOERL and subpackages/modules used for solving binary optimization problems\n",
    "import doerl\n",
    "from doerl.optimization import optimization\n",
    "from doerl.utility import utility\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Utility/Objective functions to optimize\n",
    "def utility_func_increasing(design, **kwargs):\n",
    "    \"\"\"\n",
    "    Evaluate the value of the utility function corresponding to the passed design\n",
    "    This is a trivial function that returns the sum of entries in the design. \n",
    "        Thus, the optimal solution has all entries ON (upon maximization).\n",
    "    \"\"\"\n",
    "    # Convert to boolean (we only consider binary designs here)\n",
    "    design = np.asarray(design, dtype=bool)    \n",
    "    result = np.count_nonzero(design)\n",
    "    return result\n",
    "    \n",
    "def utility_func_split(design, **kwargs):\n",
    "    \"\"\"\n",
    "    Evaluate the value of the utility function corresponding to the passed design\n",
    "    This is a trivial function that returns the number of active entries if less than or equal to design size divided by 2,\n",
    "        otherwise it returns number of inactive entries. Thus, objective value is 0, 1, ..., design size // 2.\n",
    "    This adds nonsmoothness, and make the solution non-degenerate (all designs with ~50% active/ON entries are optimal for max problems)\n",
    "    \"\"\"\n",
    "    # Convert to boolean (we only consider binary designs here)\n",
    "    design = np.asarray(design, dtype=bool)    \n",
    "    n_active = np.sum(design)\n",
    "    if n_active <= int(design.size//2):\n",
    "        result = n_active\n",
    "    else:\n",
    "        result = design.size - n_active\n",
    "    return result\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Settings:\n",
    "DESIGN_SIZE        = 10         # problem dimension; e.g., number of candidate sensors\n",
    "MAXIMIZE_OBJECTIVE = True       # Maximize if True, otherwise Minimize (see below)\n",
    "BASELINE           = 'optimal'  # Reduce variance of the stochastic estimtor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Choose the function to optimize (min by default)\n",
    "if MAXIMIZE_OBJECTIVE:\n",
    "    U = lambda d: -utility_func_split(d)\n",
    "    # U = lambda d: -utility_func_increasing(d)\n",
    "else:\n",
    "    U = lambda d: utility_func_split(d)\n",
    "    # U = lambda d: utility_func_increasing(d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set the initial policy (probability) all sensors have the same probability\n",
    "initial_p = np.ones(DESIGN_SIZE) * 0.5\n",
    "\n",
    "# Solve the optimization (min) problem using the binary REINFORCE (stochastic learning):\n",
    "optimal_policy, optimal_design_sample, optimal_design_sample_objval, \\\n",
    "    traject, converged, msg, objective_value_tracker, \\\n",
    "    sampled_design_indexes = optimization.binary_REINFORCE(U, \n",
    "                                                           init_theta=initial_p,\n",
    "                                                           # def_stepsize=0.15,\n",
    "                                                           # max_iter=15,\n",
    "                                                           # opt_sample_size=10,\n",
    "                                                           baseline=BASELINE,\n",
    "                                                           verbose=False,\n",
    "                                                          )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print results (nicely)\n",
    "print(f\"The optimal policy is: {str(optimal_policy)}\")\n",
    "print(f\"Sampled Designs (optimal sample):\")\n",
    "for xi, u_xi in zip(optimal_design_sample, optimal_design_sample_objval):\n",
    "    opt_U = -u_xi if MAXIMIZE_OBJECTIVE else u_xi\n",
    "    print(f\"Design (d): {str(xi.astype(int))}; U(d): {opt_U}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Simple functions for enumeration and other useful tasks are provided\n",
    "\n",
    "# E.g., solution by bruteforce (for reasonably small problems) to test quality of the solution\n",
    "if DESIGN_SIZE <= 15:\n",
    "    bruteforce_results = optimization.binary_bruteforce_objective(U, num_candidates=DESIGN_SIZE)\n",
    "    bruteforce_objvals = [bruteforce_results[k] for k in bruteforce_results.keys()]\n",
    "    opt_objval         = np.min(bruteforce_objvals)\n",
    "    opt_U              = -opt_objval if MAXIMIZE_OBJECTIVE else opt_objval\n",
    "    \n",
    "    # Print all designs along with evaluted objective found by bruteforce\n",
    "    print(\"Bruteforce Results (Binary Optimization)\")\n",
    "    for index in bruteforce_results.keys():\n",
    "        objval = bruteforce_results[index]\n",
    "        if objval == opt_objval:\n",
    "            xi = utility.index_to_binary_state(index, size=DESIGN_SIZE).astype(int)\n",
    "            print(f\"Optimal Design (d): {str(xi)}; U(d): {opt_U}\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
