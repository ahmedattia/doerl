
import sys

python_version = (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)
if python_version >= (3, 0, 0):
    pass
else:
    print("...\nThis package (DOERL) is optimized for Python 3.x")
    print("This version will work, but performance might face some hickups. \n\t%s\n..." % sys.version)

