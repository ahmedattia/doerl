

# Everything here, assumes we are doing stochastic binary optimization.
# Some implementations may be more general, but it's safe to say everything is tailored
# to serve our stochastic approach to binary OED.

import re
import os

import numpy as np
import itertools

from scipy import optimize as sp_optimize

from ..utility import utility
from ..stats import bernoulli as s_bernoulli



# ****** Generl-Purpose Functions ****** #
def backtracking_line_sesarch(x, f, g, max_step=1, d=None, tau=0.9, c=0.5, max_iter=10000):
    """
    Implement the backtracking line search algorithm

    Args:
        x: current state
        f: objective function that accepts x, and return f(x)
        g: the gradient function
        d: search direction; if None, will be set to g
        tau:
        c:

    Returns:
        step_size: the step size satesfying Armijo condition

    """
    assert 0 < c < 1, "c must be in the interval (0, 1)"
    assert 0 < tau < 1, "tau must be in the interval (0, 1)"

    # The gradient
    grad = g(x)

    # Local search direction
    if d is None: d = g
    m = np.dot(g, d)

    t = - c * m
    j = 0  # iteration
    alpha = max_step
    while j < max_iter:
        f0 = f(x)
        f1 = f(x + alpha * d)
        if (f0 - f1) >= alpha * t:
            print("Terminating; found optimal step size: %f" % alpha)
            break
        else:
            alhpa *= tau
            j += 1
    if j == max_iter:
        print("Maximum number of iterations reached; terminating...")
    return alpha

def box_projection(theta, l=0, u=1):
    """
    """
    assert isinstance(theta, np.ndarray), "theta must be numpy array"

    out = theta.copy()
    out[out<l] = l
    out[out>u] = u
    return out

def projected_gradient(init_x, grad, step_size, l=None, u=None, verbose=False):
    """
    Projection operator of the gradient g on bound constraints, given a state,
    gradient, and a step size
    Initial implementation uses piecewise search of bended segments

    Args:
        init_x: current state
        grad: direction
        step_size: (maximum) step size
        l: lower bounds; if None, will be set to -infty
        u: upper bounds; if None, will be set to +infty

    Returns:
        y: init_x - step_size * grad
        proj_y: Proj(y)
        breakpoints: the points x(t), inside, or at the boundarys, projected
            into the feasible domain, given the box constraints

    Remarks:
        - both l, u, must be of the same type/size/shape as x, or None
        - Currently, the local minima is lookedup only at the kinks/breakpoints
        - References:
            See J. Nocedal and S. Write "Numerical Optimization 2nd. ed." Sec 16.7

    """
    if verbose:
        print("Projected Gradient Step")
        print("Input x: %s" % init_x)
        print("Gradient: %s" % grad)
        print("Step-size: %f" % step_size)

    # Local copy, and validate input
    x0 = np.array(init_x).flatten().copy()
    g = np.array(grad).flatten().copy()
    max_t = step_size
    # Lower, and upper bounds
    if l is None:
        l = np.empty_like(x0)
        l[:] = - np.infty
    elif utility.isnumber(l):
        l = np.ones(x0.size) * l
    if u is None:
        u = np.empty_like(x0)
        u[:] = np.infty
    elif utility.isnumber(u):
        u = np.ones(x0.size) * u
    l[np.isnan(l)] = -np.infty
    u[np.isnan(u)] = np.infty

    # Full step
    y = x0 - max_t * g
    if verbose:
        print("Full step: %s" % y)

    # valid entries, and excess
    over_locs = np.where ( y>u )[0]
    under_locs = np.where( y<l )[0]
    #
    proj_y = y.copy()
    proj_y[over_locs] = u[over_locs]
    proj_y[under_locs] = l[under_locs]
    if verbose:
        print("Fully Projected y: %s" % proj_y)

    # Find breakpoints
    locs_0 = np.intersect1d(np.where(u<np.infty)[0], np.where(g<0)[0])
    locs_1 = np.intersect1d(np.where(l>-np.infty)[0], np.where(g>0)[0])

    t_bar = np.empty_like(x0)
    t_bar[:] = np.infty
    t_bar[locs_0] = (x0[locs_0] - u[locs_0]) / g[locs_0]
    t_bar[locs_1] = (x0[locs_1] - l[locs_1]) / g[locs_1]

    filtered_t = np.unique(t_bar)
    filtered_t[np.isinf(filtered_t)] = 0
    filtered_t[filtered_t>max_t] = 0
    filtered_t = filtered_t[filtered_t>0]

    if filtered_t.size == 0:
        filtered_t = np.array([max_t])
    if verbose:
        print("T-Bar (Breakpoints Parameters): %s " % t_bar)
        print("Unique t values: %s " % filtered_t)

    def kick(x, t):
        xt = x - t_bar * g
        locs = np.where(t<=t_bar)[0]
        xt[locs] = x[locs] - t * g[locs]
        return xt

    breakpoints = []
    for tk in filtered_t:
        xt = kick(x0, tk)
        breakpoints.append(xt.copy())
        if verbose:
            print("Breakpoint at t = %f :> %s " % (tk, xt))
    if verbose:
        print("Done.\n")
    return y, proj_y, breakpoints


# ****** SAA Algorithms ****** #
def stochastic_objective(J, theta, baseline=0, indices=None, average_scale=True, inspected_designs=None):
    """
    This is local implementation of the mean function given indexes k,
    and the Bernoulli variable theta, evaluate the stochastic objective
    given the passed indices size of the design is inferred from theta
    """
    theta = np.asarray(theta).flatten()
    if indices is None:
        indices = range(1, theta.size+1)
    else:
        indices = np.asarray(indices).flatten().astype(int)

    obj_val = 0.0
    for k in indices:
        J_val = None
        design = utility.index_to_binary_state(k, size=theta.size)

        if inspected_designs is not None:
            if k in inspected_designs:
                J_val = inspected_designs[k]

        if J_val is None:
            J_val = J(design)
            if inspected_designs is not None:
                inspected_designs.update({k:J_val})

        prob = s_bernoulli.pmf(design, theta)[1]
        obj_val += prob * J_val

    if average_scale:
        obj_val /= float(indices.size)
    return obj_val

def stochastic_objective_grad(J, theta, indices=None, average_scale=True, inspected_designs=None):
    """
    This is local implementation of the gradient of the mean function
    given indexes k, and the Bernoulli variable theta,
    evaluate the stochastic objective gradient w.r.t PMF parameter (theta)
    given the passed indices size of the design is inferred from theta
    """
    theta = np.asarray(theta).flatten()
    if indices is None:
        indices = range(1, theta.size+1)
    else:
        indices = np.asarray(indices).flatten().astype(int)

    obj_grad = np.zeros(theta.size)
    for k in indices:
        J_val = None
        design = utility.index_to_binary_state(k, size=theta.size)

        if inspected_designs is not None:
            if k in inspected_designs:
                J_val = inspected_designs[k]

        if J_val is None:
            J_val = J(design)
            if inspected_designs is not None:
                inspected_designs.update({k:J_val})

        prob_grad = s_bernoulli.grad_pmf(design, theta)[1]
        obj_grad += prob_grad * J_val

    if average_scale:
        obj_grad /= float(indices.size)
    return obj_grad

def binary_SAA(J,
               init_theta,
               reset_init_theta=True,
               batch_size=32,
               verification_size=50,
               num_replica=10,
               pgtol=1e-05,
               epsilon=1e-08,
               gap_tol=1e-5,
               gap_var_tol=1e-12,
               verbose=False):
    """
    An implementation of the stochastic average approximation (SAA) algorithm
    The implementation is tailored for our project, and by no means is general.
        x here signifies the hyperparameter (\theta) in our work

    Args:
        J : the function inside the expectation of the stochastic objective,
            this function takes on argument d, and returns the value of J(d),
            where d is a design vector (binary)
        init_theta: an initial solution; must be 1d numpy array.
            The size of init_theta sets the dimensionality of the problem
        reset_init_theta: Reuse init_theta as initial solution for each the
            deterministic optimization problem at each replication.
            If False, the optimal solution from previous replication is used as
            initial soltuion.
        batch_size (N): size of sample generated for each replica to approximate
            the expectation in the stochastic objective function
        verification_size (N'):
        num_replica (M):
        pgtol=1e-05, epsilon=1e-08, passed to LBFGS for solving the deterministic problem
        gap_tol (\beta):
        gap_var_tol (S^2_{\beta}):

    Returns:
        theta_opt: the optimal solution proposed by the screening process
        obj_opt_est: an estimate of the objective value at theta_opt using
            a verification sample
        inspected_designs: a dictionary storing all inspected
            design (binary) variables (only index k) indexed by k,
            with corresponding values of \J(\design[k])
            k is the binary mapping relation from x to binary state, defined in the draft
        parameters_pool: list of optimal solutions obtained at each replica/iteration
        parameters_pool_objective:

    Remarks:
        Anton Kleywegt et.al. The sample average approximation method for stochastic discrete optimization.
        SIAM Journal on Optimization, 12(2):479, 2002.

    """
    assert isinstance(init_theta, np.ndarray), "init_theta must be a numpy array"
    assert np.ndim(init_theta) == 1, "init_theta must be 1d numpy array"

    assert batch_size > 1, "batch_size must be > 1"
    assert verification_size > 1, "verification_size must be > 1"
    assert num_replica >= 1, "num_replica must be >= 1"

    # Problem and Space sizes
    init_theta = init_theta.flatten()
    problem_size = init_theta.size
    if problem_size > 100:
        print("WARNING: the problem size is large MC methods might not be your best bet!")
    possible_choices = 2** problem_size

    # Initialization
    inspected_designs = dict()
    v_bar = 0.0
    S2 = 0.0

    # Verification sample
    verification_sample = np.random.choice(possible_choices, size=verification_size) + 1
    for k in verification_sample:
        if k not in inspected_designs:
            design = utility.index_to_binary_state(k, size=problem_size)
            j_val = J(design)
            inspected_designs.update({k:j_val})

    # Loop over replicas
    flag = 0  # convergence flag (0: failed , 1: gap tolerance met, 2: gap variance tolerance met)

    # Track the optimal solutions of the deterministic problem at each replica
    parameters_pool = []
    parameters_pool_objective = []

    for m in range(1, num_replica+1):

        # Uniformly (for now!) sample indexes
        batch_sample = np.random.choice(possible_choices, size=batch_size) + 1
        obj = lambda x: stochastic_objective(J,
                                              theta=x,
                                              indices=batch_sample,
                                              average_scale=False,
                                              inspected_designs=inspected_designs
                                             )
        obj_grad = lambda x: stochastic_objective_grad(J,
                                                        theta=x,
                                                        indices=batch_sample,
                                                        average_scale=False,
                                                        inspected_designs=inspected_designs
                                                       )
        # solve the deterministic optimization problem, with box-constraints [0, 1]
        callback = None  # TODO: verbosity
        if not reset_init_theta and m>1:
            x0 = parameters_pool[-1].copy()
        else:
            x0 = init_theta
        bounds = [(0, 1)]*problem_size
        theta_N_m, v_N_m, opt_d  = sp_optimize.fmin_l_bfgs_b(obj, x0,
                                                             fprime=obj_grad,
                                                             bounds=bounds,
                                                             pgtol=pgtol,
                                                             epsilon=epsilon,
                                                             iprint=101,
                                                             maxfun=15000,
                                                             maxiter=15000,
                                                             disp=1
                                                            )
        if opt_d['warnflag']:
            print("LBFGS failed to solve SSA replica %d " % m)
            raise ValueError

        parameters_pool.append(theta_N_m)
        parameters_pool_objective.append(v_N_m)

        # Update v_bar
        v_bar = ( v_bar*(m-1.0) + v_N_m ) / m

        # Approximate the true objective using the verification sample
        v_bar_prime = 0.0
        verification_prob = np.zeros(verification_size)
        for k_ind in range(verification_size):
            k = verification_sample[k_ind]
            J_val = inspected_designs[k]
            design = utility.index_to_binary_state(k, size=problem_size)
            prob = s_bernoulli.pmf(design, theta_N_m)[1]
            verification_prob[k_ind] = prob
            v_bar_prime += prob * J_val
        v_bar_prime /= verification_size

        # Update S2
        if m > 1:  # for m <= 1, variance is 0
            S2 = (S2 * (m-2.0) + (v_N_m - v_bar)**2) / (m-1.0)

        # Estimate the optimality gap
        beta = v_bar_prime - v_bar

        # Estimate optimality gap variance
        S2_beta = 0.0
        for k_ind in range(verification_size):
            k = verification_sample[k_ind]
            J_val = inspected_designs[k]
            prob = verification_prob[k_ind]
            S2_beta += (J_val*prob - v_bar_prime)**2
        S2_beta /= verification_size*(verification_size-1.0)
        S2_beta += S2 / m

        # Compare optimality gap and gap variance to passed tolerance
        if beta < gap_tol:
            print("Replica [%d] Convergence based on optimality gap estimation" % m)
            flag = 1
            break
        if beta < gap_tol:
            print("Replica [%d] Convergence based on optimality gap variance estimation" % m)
            flag = 2
            break

    if flag == 0:
        print("Optimizer didn't converge; returning best state out of the pool")

    # screening and selection
    winner = np.argsort(parameters_pool_objective)[0]
    theta_opt = parameters_pool[winner]
    obj_opt_est = parameters_pool_objective[winner]
    parameters_pool.insert(0, init_theta.copy())
    parameters_pool_objective.insert(0, np.nan)

    return theta_opt, obj_opt_est, inspected_designs, parameters_pool, parameters_pool_objective


# ****** Optimal Policy (REINFORCE) Algorithms ****** #
def stepsize_schedule(itr, def_step=1, method='constant'):
    """
    Return a suitable step size at a given optimization iteration

    Args:
        itr: iteration numer (positive integer)
        def_step: default step size (see Remarks)
        method: approach to choose the step size at a given iteration (see Remarks)

    Remarks:
        Step size is determined based on the selected method
        'constant': def_step is used at all iterations
        'decay': def_step/iter

    """
    assert (itr > 0), "Iteration number must be positive!"
    assert (def_step > 0), "Default step must be positive!"

    if re.match(r'\Aconst(ant)*\Z',method, re.IGNORECASE):
        stepsize = def_step
    elif re.match(r'\Ade(creas(e|ing)*|cay(ing)*)\Z',method, re.IGNORECASE):
        stepsize = def_step / float(itr)
    else:
        print("Unrecognized method %s " % method)
        raise ValueError
    return stepsize

def exact_policy_gradient(J, theta, apply_projection=False, verbose=False):
    """
    Given the current parameter (theta), evaluate the exact value of the gradient

    Args:
        theta: the current policy
        J: the objective function in the original formulation; accepts a binary design,
            and return value of of the objective

    Returns:
        grad: exact policy gradient

    """
    # print("Calculating Exact Gradient...")

    theta = np.array(theta).flatten()
    size = theta.size

    # Enumerate all possible designs
    # possible_designs = utility.enumerate_binary(size)

    # Initialize gradient, and accumulate objective terms' gradients
    grad = np.zeros(size)
    grad_update = np.zeros(size)
    for k in range(1, 2**size+1):
        w = utility.index_to_binary_state(k, size=size)
        pgrad = s_bernoulli.grad_pmf(w, theta)[1]
        grad_update[:] = J(w) * pgrad
        grad += grad_update
    if apply_projection:
        grad = box_projection(grad, l=-1, u=1)
    return grad

def stochastic_policy_gradient(J,
                               theta,
                               design_sample=None,
                               batch_size=32,
                               epochs=1,
                               apply_projection=False,
                               antithetic=False,
                               verbose=False):
    """
    Given the current parameter (theta), evaluate the policy stochastic gradiet
        by sampling the current plicy, and using the kernel trick
    The gradient is approximated using one sample (if epochs==1), or as mean of means (if epochs > 1)

    Args:
        theta: the current policy
        J: the objective function in the original formulation; accepts a binary design,
            and return value of of the objective

    Returns:
        grad: stochastic policy gradient

    Remarks:
        If more than one epoch is required:
            - if design_sample is None, batch_size is generated for each epochs
            - if design_sample is not None, batch_size is derived as: design_sample // epochs

    """
    if verbose:
        print("Calculating Stochastic Gradient...")
        print("Sample size: %d" % batch_size)
        print("Epochs: %d" % epochs)
        print("Apply projection: %s" % apply_projection)
        print("Use Antithetic variates: %s" % antithetic)

    theta = np.array(theta).flatten()
    observation_dimension = theta.size

    epochs = int(epochs)
    if epochs > 1:
        if design_sample is not None:
            batch_size = len(design_sample) // epochs
    else:
        if design_sample is not None:
            batch_size = len(design_sample)

    grad = np.zeros(observation_dimension)
    for epoch in range(epochs):
        epoch_grad = np.zeros(observation_dimension)
        # Sample designs from given the passed parameters
        if design_sample is None:
            sample = s_bernoulli.sample(theta,
                                        batch_size,
                                        antithetic=antithetic)
        else:
            sample = design_sample[epoch*batch_size: (epoch+1)*batch_size]
        # sample_indexes = [utility.index_from_binary_state(s) for s in sample]

        for w in sample:
            # RL-based stochastic gradient
            grad_update = J(w) * s_bernoulli.grad_log_pmf(w, theta)[1]
            epoch_grad += grad_update
        epoch_grad /= float(len(sample))

        grad += epoch_grad
    grad /= float(epochs)

    grad[np.isnan(grad)] = 0  # for zero probability
    grad[theta==0] = 0
    grad[theta==1] = 0
    # grad[np.isinf(grad)] = 0  # for zero probability

    if apply_projection:
        grad = box_projection(grad, l=-1, u=1)
    if verbose:
        print("Stochastic gradient evaluted. returning...")
    return grad

def optimal_baseline_estimate(J,
                              theta,
                              sample=None,
                              batch_size=32,
                              epochs=10,
                              antithetic=True,
                              verbose=False):
    """
    Use the passed sample to calculate sample based estimate of the optimal
    baseline value
    """
    #
    theta = np.array(theta).flatten()
    dimension = theta.size

    if sample is None:
        # kill sample, and resample
        # Resample, and average
        assert epochs >= 1, "Number of epochs must be at least 1"

        sample_size = batch_size
        ghatT_d = 0.0
        d = np.zeros(dimension, dtype=np.float)
        ghat = np.zeros(dimension, dtype=np.float)
        for epoch in range(epochs):
            d[...] = 0.0
            ghat[...] = 0.0
            sample = s_bernoulli.sample(theta, sample_size, antithetic=antithetic).astype(int)
            for i in range(sample_size):
                grad_log = s_bernoulli.grad_log_pmf(sample[i], theta)[1]
                # print("grad_log", sample[i], theta, grad_log)
                #
                d += grad_log
                ghat += J(sample[i]) * grad_log
            # mean of d, and g
            d /= float(sample_size)
            ghat /= float(sample_size)
            #
            ghatT_d += np.dot(d, ghat)
            # print("theta... ghatT_d", theta, ghatT_d)
        baseline = ghatT_d / float(epochs)

    else:
        # Ignore batch_size, epochs
        sample_size = len(sample)

        d = np.zeros(dimension)
        for i in range(sample_size):
            d += s_bernoulli.grad_log_pmf(sample[i], theta)[1]

        baseline = 0.0
        for i in range(sample_size):
            g = J(sample[i]) * s_bernoulli.grad_log_pmf(sample[i], theta)[1]
            baseline += d.dot(g)
        baseline /= float(sample_size)

    #
    # Scale
    valid_inds = np.intersect1d(np.where(theta>0)[0], np.where(theta<1)[0])
    valid_probs = theta[valid_inds]
    if verbose:
        print("OPtimal Baseline Calculation:")
        print("Num valid inds: %d" % valid_inds.size)
        print("Valid Probabilities: %s" % valid_probs)
        print("E[gT d]: %f" % baseline)
    if valid_probs.size > 0:
        if verbose:
            # print("Valid-Probs: ", valid_probs)
            print("sum(1.0/(theta-theta^2)): ", np.sum(1.0/(valid_probs-valid_probs**2)))
        scl =  sample_size / np.sum(1.0/(valid_probs-valid_probs**2))
        baseline *= scl
        if verbose:
            print("Baseline: %f" % baseline)
    else:
        baseline*= sample_size  #  = 0.0
        # baseline= 0.0
    # baseline = max(0, baseline)
    return baseline

def binary_REINFORCE(J,
                     init_theta,
                     def_stepsize=1.0,
                     Newton_step=False,
                     decay_step=False,
                     batch_size=32,
                     opt_sample_size=1,
                     objective_value_tracker=None,
                     max_iter=1000,
                     pgtol=1e-8,
                     baseline='huristic',
                     monitor_iterations=True,
                     antithetic=False,
                     verbose=False):
    """
    An implementation of the REINFORCE algorithm for stochastic binary optimization ::math:`\min_{d} E[J(d)]`

    Args:
        J: the function (to be minimized) inside the expectation of the stochastic objective,
                  this function takes on argument d, and returns the value of J(d),
                  where d is a design vector (binary)
        init_theta: an initial solution; must be 1d numpy array.
                           The size of init_theta sets the dimensionality of the problem
        def_stepsize=1: learning rate; if None, automatic step size may be involved!
        decay_step: whether to decrease the step size as the algorithm proceeds or not
        batch_size (N): size of sample generated for each replica to approximate
                               the expectation in the stochastic objective function
        opt_sample_size: number of design samples to generate using the optimal solution
        param max_iter: maximum number of iterations
        pgtol: threshold for convergence (norm of projected gradient)
        baseline: the baseline used to reduce variance of the stochastic estimator of the gradient; accepted values:
            - None: no baseline is used
            - 'huristic': evaluate an empirical baseline ::math:`(J(0)+J(1))/2`
            - 'optimal': a constant baseline is found by approximating the optimal baseline where optimality is defined as minimum variability of the estimator.
        monitor_iterations: save the results at each iterations
        verbose: screen/file verbosity
        file_out_dir: Path to folder (will be created if doesn't exist) in which to write reports, output, etc. If `None`, no file output is reported. (log-files are shut off)

    Returns:
        optimal_theta: the parameters of the optimal policy (probability of activation of each entry of the design)
        optimal_sample: design sample generated by sampling the optimal policy (Bernoulli samples with probababilities set to the optimal policy parameters `optimal_theta`)
        optimal_sample_objval:
        traject: trajectory (consecutive parameters) of the policy parameters generated along the optimization iterations
        converged: `True` if the algorithm converged, `False` otherwise
        msg: a string message describing the final status of the algorithm
        objective_value_tracker: a dictionary containing the value of the objective function evaluated through the course of the algorithm. The dictionary is indexed by the integer indexes of the generated binary states.
        sampled_design_indexes: a list (of lists) containing all sampled design indexes for each iteration of the algorithm. You can generate the binary designs themselves by calling `utility.index_to_binary_state()` on the indexes

    Remarks:
        Steps of the algorithm are as follows:
            1- Start with the initial state
            2- Calculate the stochastic gradient with/without baseline
            3- Optionally, calculate Hessian (approximation)
            4- Take a step in the direction of -stepsize * gradient

        The algorithm can be used to solve a maximization instead of minimization simply by altering the sign of the objective.
            This is helpful for formulations where one is interested in maximizing a utility function such as KL divergence or information gain.
    """
    init_theta = np.array(init_theta).flatten()
    dimension = init_theta.size
    if np.any(init_theta<0) or np.any(init_theta>1):
        print("The parameter values set in init_theta are invalid; "
              "some of them are outside the inerval [0, 1]")
        raise ValueError

    log_file = utility.try_file_name(directory=os.path.abspath(os.getcwd()),
                                     file_prefix="__Binary_REINFORCE_LOG",
                                     extension="txt")
    with open(log_file, 'w') as f_id:
        msg = "Binary Stochastic (REINFORCE) Optimization Algorithm Log\n"
        msg += "\nParameters: \n %s\n" % ("="*20)
        msg += "def_stepsize : %f \n" % def_stepsize
        msg += "Newton_step  : %s \n" % Newton_step
        msg += "decay_step   : %s \n" % decay_step
        msg += "batch_size   : %d \n" % batch_size
        msg += "max_iter     : %d \n" % max_iter
        msg += "pgtol        : %f \n" % pgtol
        msg += "baseline     : %s \n" % baseline
        msg += "antithetic   : %s \n" % antithetic
        msg += "%s\n" % ("="*20)
        f_id.write(msg)

    # Track objective values for indexed designs
    if objective_value_tracker is None:
        objective_value_tracker = dict()
    def local_objective(w):
        """ Prevent redundant computations of the objective """
        k = utility.index_from_binary_state(w)
        msg = "Evaluate Objective Value for design [%d]" % k

        if k in objective_value_tracker:
            msg += "\nFound in history; Loading."
            if verbose: print(msg)
            j_val = objective_value_tracker[k]
        else:
            msg += "\nNot found, calcualating"
            if verbose: print(msg)
            j_val = J(w)
            objective_value_tracker.update({k:j_val})

        msg += "\nObjective Value = %f\n--------\n" % j_val
        with open(log_file, 'a') as f_id:
            f_id.write(msg)

        return j_val

    # Validate the baseline (constant/function)
    if baseline is None:
        baseline_value = 0
    elif re.match(r'\Ahuristic|empirical\Z', baseline, re.IGNORECASE):
        # Determine huristically
        b_max = local_objective(np.zeros_like(init_theta, dtype=bool))
        b_min = local_objective(np.ones_like(init_theta, dtype=bool))
        b_avg = (b_min + b_max / 2.0)
        baseline_value = b_avg
    elif re.match(r'\Aoptimal\Z', baseline, re.IGNORECASE):
        baseline_value = None
    else:
        print("Unrecognized baseline")
        raise ValueError

    # Define proper delta functions
    # The objective function (with baseline)  (Overwrites J)
    # TODO: Need to keep track of indexed at which objective is evaluated...
    # J_baseline = lambda design: J(design) - baseline(*baseline_args)

    # Stepsize schedule
    stepsize = lambda itr: stepsize_schedule(itr, def_stepsize, 'decay' if decay_step else 'constant')


    # Start Algorithm Iterations
    converged = False
    iter_ind = 0
    theta = init_theta.copy()
    traject = [theta.copy()] if monitor_iterations else []

    # each entry of the following list contains indexes of sampled designs at each iteration
    sampled_design_indexes = []

    print("Applying the stochastic optimization algorithm...")
    while not converged:
        if iter_ind >= max_iter:
            msg = "Maximum number of iterations exceeded without convergence\n"
            msg += "Returning the last obtained set of parameters\n"
            if  verbose: print(msg)
            with open(log_file, 'a') as f_id:
                f_id.write(msg)

            msg = "MAXIMUM NUMBER OF ITERATIONS REACHED"
            break
        else:
            iter_ind += 1

        # At this iteration, calculate the stochastic gradient,
        # Sample bernoulli distribution
        sample = s_bernoulli.sample(theta, batch_size, antithetic=antithetic)

        # Keep track of sampled indices
        sample_indexes = [utility.index_from_binary_state(s) for s in sample]
        sampled_design_indexes.append(sample_indexes)

        if baseline_value is None:
            # optimal baseline
            # b = optimal_baseline_estimate(local_objective, theta=theta, sample=sample)
            b = optimal_baseline_estimate(local_objective, theta=theta, sample=None,
                                          antithetic=antithetic)  # <<--use replicas
            # print("Optimal baseline estimate: %f " % b)
        else:
            b = baseline_value
        J_func = lambda design: local_objective(design) - b

        # estimate baseline:
        stoch_grad = stochastic_policy_gradient(J_func,
                                                theta,
                                                design_sample=sample,
                                                batch_size=batch_size,
                                                epochs=1,
                                                verbose=verbose)

        # Get a step size for this iteration, take a step, and project
        # Here, take a step, then project
        if Newton_step:
            # TODO: Solve lienar system instead; just testing for now
            Hess = full_Hessian_estimate(J_func, theta)
            Hess_inv = np.linalg.inv(Hess)
            new_theta = box_projection(theta - stepsize(iter_ind)*Hess_inv.dot(stoch_grad))
        else:
            new_theta = box_projection(theta - stepsize(iter_ind)*stoch_grad)

        # Check the updated state, and check convergence, and terminate or
        update = new_theta - theta
        update_norm = np.linalg.norm(update)

        proj_grad = box_projection(stoch_grad, l=-1, u=1)
        proj_grad_norm = np.linalg.norm(proj_grad)
        # print("Stoch Gradient", stoch_grad)
        # print("Proj- Gradient", proj_grad)
        # Maybe, we can project gradient, take step, then project!

        # Print sutff to screen
        # print("STEEPEST DESCENT UPDATE: ", theta, new_theta, stoch_grad)
        out = "REINFORCE Iteration: %d ; Proj-Grad-Norm: %f ; Step-Update-Norm: %f\n" % (iter_ind,
                                                                     proj_grad_norm,
                                                                     update_norm)
        if verbose: print(out)
        with open(log_file, 'a') as f_id:
            f_id.write(out)
        # print("Steepeste-Descent-Proj-Grad: ", proj_grad, "New Theta: %s", new_theta)

        # Update theta
        theta = new_theta.copy()
        if monitor_iterations:
            traject.append(theta.copy())

        if update_norm <= pgtol:
            converged = True
            msg = "CONVERGED: PROJECTED GRADIENT NORM < PGTOL"
    msg += "\nNumber of Iterations: %d " % iter_ind
    msg += "\n Number of Function Evaluations (Post Trace): %d" % len(objective_value_tracker)

    # Just rename the optimal/final theta/policy-parameter
    optimal_theta = theta

    # Given the optimal theta, sample the optimal policy,
    optimal_sample = s_bernoulli.sample(optimal_theta, opt_sample_size)
    optimal_sample_objval = [local_objective(w) for w in optimal_sample]

    # File and screen output
    with open(log_file, 'a') as f_id: f_id.write(msg)
    if verbose:
        print(msg)
    else:
        print("done...")

    # Done; return
    return optimal_theta, optimal_sample, optimal_sample_objval, \
        traject, converged, msg, objective_value_tracker, sampled_design_indexes

# ****** #
def Hessian_estimate_row(J, row_ind, theta,
                          design_sample=None, def_sample_size=32, antithetic=False):
    """
    Given the function J, a row index, theta and design, and possible
    """
    theta = np.array(theta).flatten()

    assert 0 <= row_ind < theta.size, "Row index out of range!"
    dimension = theta.size

    # Sample if not sample found
    if design_sample is None:
        design_sample = s_bernoulli.sample(theta, def_sample_size, antithetic=antithetic)
    sample_size = len(design_sample)

    invalid_inds = np.union1d(np.where(theta==0), np.where(theta==1))

    theta_recip = 1.0 / theta
    theta_recip[invalid_inds] = 0.0

    theta_inv_recip = 1.0 / (1.0 - theta)
    theta_inv_recip[invalid_inds] = 0.0

    # Initiate
    res = np.zeros(dimension)
    update = np.empty(dimension)

    for design in design_sample:

        update = design * theta_recip - (1.0 - design) * theta_inv_recip
        update *= design[row_ind]*theta_recip[row_ind] - (1.0-design[row_ind])*theta_inv_recip[row_ind]
        update *= J(design)
        update[row_ind] = 0

        res += update

    res /= float(sample_size)

    return res

def Hessian_estimate_vector_prod(J, theta, x, alpha=1.0, design_sample=None,
                                 def_sample_size=32, antithetic=False):
    """
    (alpha * I + Hessian ) * x
    """
    theta = np.array(theta).flatten()
    dimension = theta.size

    # Sample if not sample found
    if design_sample is None:
        design_sample = s_bernoulli.sample(theta, def_sample_size, antithetic=antithetic)
    sample_size = len(design_sample)

    res = np.empty(dimension)

    for row_ind in range(dimension):
        # Estimate Hessian row
        hess_row = Hessian_estimate_row(J=J,
                                         row_ind=row_ind,
                                         theta=theta,
                                         design_sample=design_sample)
        hess_row[row_ind] += alpha

        res[row_ind] = hess_row.dot(x)

    return res

def full_Hessian_estimate(J, theta, alpha=1.0, sample_size=32, antithetic=False):
    """
    """
    theta = np.array(theta).flatten()
    dimension = theta.size

    design_sample = s_bernoulli.sample(theta, sample_size, antithetic=antithetic)

    Hess = np.empty((dimension, dimension))
    e = np.zeros(dimension)
    for row_ind in range(dimension):
        e[...] = 0.0
        e[row_ind] = 1.0
        Hess[row_ind, :] = Hessian_estimate_vector_prod(J=J,
                                                        theta=theta,
                                                        x=e,
                                                        design_sample=design_sample,
                                                        alpha=alpha)
    return Hess


# ****** #
def binary_bruteforce_objective(obj,
                                num_candidates,
                                num_active=None,
                                seed_tracker=None,
                                verbose=False):
    """
    Evaluate  the value  of the objective (obj) at all possible binary combinations;

    Args:
        obj:
        num_candidates: objective space dimension (design  space)
        num_active: If None, all sizes will be looped over, otherise
            must be
                a) an integer <= num_candidates), only solutions with  active subset
                    (==1) equal to num_active will be calculated.
                b) list of integers (looped  over as in (a) above)
        seed_tracker: an optional dictionary with partial information to lookup before evaluation
            Keys are integer indexes of binary candidates.
        verbose: print things to screen or not

    Returns:
        An updated veresion of seed_tracker with additional  results;
        If seed_tracker is passed, it will be overwritten (in-place)

    """
    # All possible combinations;
    # Better use combinations if num_active is None
    combs = 2**num_candidates

    if num_active is None:
        pass
    elif isinstance(num_active, int):
        msg = "num_active must be a non-negative integer less than the number of possible candidates"
        assert 0 <= num_active <= num_candidates, msg
        num_active = [num_active]
    elif isinstance(num_active, list):
        pass
    else:
        print("None, scalar, or a list is expected here; found %s" % type(num_active))
        raise TypeError

    if seed_tracker is None:
        seed_tracker = dict()

    # Full bruteforce
    for k in range(1, combs+1):
        if verbose: print("Candidate %08d / %08d" % (k, combs))
        if k in seed_tracker:
            if verbose: print("Found in Dictionary; passing")
            continue
        else:
            design = utility.index_to_binary_state(k, size=num_candidates)
            if num_active is not None:
                if np.count_nonzero(design) not in num_active:
                    if verbose: print("Wrong number of active entries; passing")
                    continue
            j = obj(design)
            seed_tracker.update({k:j})

    if verbose: print("Done.")
    return seed_tracker

def bruteforce_solution(obj, num_candidates, cluster_by_size=True, seed_tracker=None):
    """
    Solve the OED problem by enumeration

    Args:
        obj:
        num_candidates:
        cluster_by_size:

    Returns:
        candidates: either indexes of possible solutions (k=1:2^Nsens), or
            sizes of candidate solutions
            if the latter, candidate length is num_candidates+1
        objective_values: list with objective values of same length as candidates;
            if cluster by size, each entry is a list
            if not cluster by size, each entry is a scalar

    """
    combs = 2**num_candidates

    if seed_tracker is None:
        seed_tracker = dict()

    # Full bruteforce
    candidates = [i for i in range(1, combs+1)]
    objective_values = [None] * len(candidates)
    print("Brute-Force Search")
    for k in range(1, combs+1):
        try:
            j = seed_tracker[k]
        except(KeyError):
            design = utility.index_to_binary_state(k, size=num_candidates)
            j = obj(design)
            seed_tracker.update({k:j})
        objective_values[k-1] = j
        print("\r Candidate %08d / %08d" % (k, combs)),

    if cluster_by_size:
        candidates, objective_values = utility.group_by_num_active(seed_tracker, num_candidates)

    print("\nDone.")

    return candidates, objective_values, seed_tracker


# ****** #





